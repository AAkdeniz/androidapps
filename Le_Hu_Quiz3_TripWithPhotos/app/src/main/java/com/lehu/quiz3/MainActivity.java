package com.lehu.quiz3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.toString();
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

    ListView lvTripList;
    List<Trip> tripsList;
    ArrayAdapter<Trip> tripArrayAdapter;
    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        appDatabase = AppDatabase.getInstance(getApplicationContext());
        // init listview and init friendlist to show it at UI.
        lvTripList = (ListView) findViewById(R.id.lvTripList);
        tripsList = new ArrayList<>();
        //init adapter with a layout I created and a friendlist;
        tripArrayAdapter = new TripArrayAdapter(this, tripsList);
        lvTripList.setAdapter(tripArrayAdapter);

    }


    @Override
    protected void onStart() {
        super.onStart();
        new FethcAllDataAsyncTask().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itAdd:
                Log.d(TAG, "onOptionsItemSelected: item of add is click");
                //todo: create an new acitivy to add a new friend;
                Intent intent = new Intent(this, AddTripActivity.class);
                startActivity(intent);
                break;

            case R.id.itURL:
                Log.d(TAG, "onOptionsItemSelected: item Url");
                Intent intent1 = new Intent(this, URLActivity.class);
                startActivity(intent1);//todo: startActivityorREsult
                break;

            case R.id.itSortedByDestination:
                Log.d(TAG, "onOptionsItemSelected: item sorted by destion");
                sortedByDestination();
                break;
            case R.id.itSortedByDate:
                Log.d(TAG, "onOptionsItemSelected: item sorted by date");
                sortedByDate();
                break;

            default:
                //do nothing;
        }
        tripArrayAdapter.notifyDataSetChanged();
        return true;
    }

    private void sortedByDate() {
        Collections.sort(tripsList, new Comparator<Trip>() {
            @Override
            public int compare(Trip o1, Trip o2) {
                return Long.compare(o1.getDueDate(), o2.getDueDate());
            }
        });


    }

    private void sortedByDestination() {

        Collections.sort(tripsList, new Comparator<Trip>() {
            @Override
            public int compare(Trip o1, Trip o2) {
                return o1.getDestination().compareTo(o2.getDestination());
            }
        });

    }

    //class adapter;

    class TripArrayAdapter extends ArrayAdapter<Trip> {

        private Context context;
        private List<Trip> list;

        public TripArrayAdapter(Context context, List<Trip> arraylist) {
            super(context, R.layout.trip_item, arraylist);
            this.context = context;
            this.list = arraylist;
        }


        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View viewTask = convertView;
            if (viewTask == null) {
                viewTask = inflater.inflate(R.layout.trip_item, parent, false);
            }
            ImageView ivIcon = (ImageView) viewTask.findViewById(R.id.ivPhoto);
            TextView tvFirstLine = (TextView) viewTask.findViewById(R.id.tvDestination);
            TextView tvSecondLine = (TextView) viewTask.findViewById(R.id.tvDate);
            //
            Trip trip = list.get(position);
            tvFirstLine.setText(trip.getDestination());
            String dueDate = "Due: " + simpleDateFormat.format(new Date(trip.getDueDate()));
            tvSecondLine.setText(dueDate);
            Log.d(TAG, "getView: filepath : "+ trip.getImageFilePath());
            if (trip.getImageFilePath() != null && !trip.getImageFilePath().equals(" ")) {
                Uri bitmap = Uri.fromFile(new File(trip.getImageFilePath()));
                ivIcon.setImageURI(bitmap);
            }
            return viewTask;
        }
    }

    class FethcAllDataAsyncTask extends AsyncTask<Void, Void, List<Trip>> {
        @Override
        protected List<Trip> doInBackground(Void... voids) {
            return appDatabase.daoAccess().fetchAll();
        }

        @Override
        protected void onPostExecute(List<Trip> trips) {
            // super.onPostExecute(trips);
            tripsList.clear();
            tripsList.addAll(trips);
            tripArrayAdapter.notifyDataSetChanged();
        }
    }
}
