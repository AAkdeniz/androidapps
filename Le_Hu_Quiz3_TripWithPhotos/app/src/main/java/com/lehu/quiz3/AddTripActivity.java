package com.lehu.quiz3;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddTripActivity extends AppCompatActivity {

    private static final String TAG = AddTripActivity.class.toString();

    private AppDatabase appDatabase;
    //----define var to UI;
    EditText etDestination;
    EditText etDate;
    //image
    public static final String IMAGE_DIR = "/galleries";

    private int REQCODE_GALLERY = 1, REQCODE_CAMERA = 2;
    ImageView imageView;

    private RequestPermissionHandler permissionHandler;

    //    String imageFilePaht;
    String imageFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip);

        appDatabase = AppDatabase.getInstance(getApplicationContext());

        etDestination = (EditText) findViewById(R.id.etDestination);
        etDate = (EditText) findViewById(R.id.etDate);
        imageView = (ImageView) findViewById(R.id.ivPhoto);

        permissionHandler = new RequestPermissionHandler();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_save:
                Log.d(TAG, "onOptionsItemSelected: save a friend to db ");
                //todo: save
                addTrip();
                break;
        }
        return true;
    }


    private void addTrip() {
        Trip newTrip = new Trip();
        newTrip.setDestination(etDestination.getText().toString());
        String[] strings = etDate.getText().toString().split("/");
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.YEAR, Integer.parseInt(strings[0]));
        gc.set(Calendar.MONTH, Integer.valueOf(strings[1]) - 1);
        gc.set(Calendar.DAY_OF_MONTH, Integer.valueOf(strings[2]));
//        date = gc.getTime();
        newTrip.setDueDate(gc.getTime().getTime());
        //todo: save image view file path;
        newTrip.setImageFilePath(imageFilePath);
        new AddFriendAsyncTask().execute(newTrip);
        finish();
    }

    class AddFriendAsyncTask extends AsyncTask<Trip, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Trip... friends) {
            try {
                appDatabase.daoAccess().insertTrip(friends[0]);
                return true;
            } catch (Exception ex) {
                return false;
            }
        }
    }


    public void onClickImgView(View view) {
        Log.d(TAG, "onClickImgView: ");
        this.showPictureDialog();
    }


    /**
     * Show a dialog with select options
     */
    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        choosePhotoFromGallery();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;
                }
            }
        });
        pictureDialog.show();
    }


    /**
     * Select image from gallery
     */
    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQCODE_GALLERY);
    }

    /**
     * Capture image from camera
     */
    private void takePhotoFromCamera() {
        permissionHandler.requestPermission(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                123, new RequestPermissionHandler.RequestPermissionListener() {

                    @Override
                    public void onSuccess() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQCODE_CAMERA);
                        Toast.makeText(AddTripActivity.this, "Request permission success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailed() {
                        Toast.makeText(AddTripActivity.this, "Request permission failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == REQCODE_GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    imageFilePath = path;
                    Toast.makeText(AddTripActivity.this, "Image Saved!", Toast.LENGTH_LONG).show();
                    imageView.setImageBitmap(bitmap);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Toast.makeText(AddTripActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == REQCODE_CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(thumbnail);
            imageFilePath = saveImage(thumbnail);
            Toast.makeText(AddTripActivity.this, "Image Saved!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Save image to default media directory with timestamp
     *
     * @param bitmap
     * @return
     */
    private String saveImage(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIR);

        // Have the object build the directory structure, if needed
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File file = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fileOutputStream.close();
            Log.d(TAG, "File Saved: ==>" + file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "Failed to create file", ex);
        }
        return " ";
    }
}
