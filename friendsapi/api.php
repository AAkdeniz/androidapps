<?php

require_once 'vendor/autoload.php';

//if(true){
//    DB::$dbName = "id8105401_friendsdb3306";
//    DB::$user = "id8105401_friendsdb3306";
//    DB::$name ="*HHh^WYR*HuKAjlgd(aF";
//
//}
//else{
DB::$dbName = 'friendsdb';
DB::$user = 'friendsdb';
DB::$password = "friendsdb";
DB::$host = '127.0.0.1'; // 127.0.0.1 if it doesn't work
DB::$port =3306;
//}

$app = new \Slim\Slim();

// this script will always return JSON  to any client
$app->response->header('content-type','application/json');
        
$app->get('/friends', function (){
    $friendsList = DB::query("SELECT * FROM friends");
    echo json_encode($friendsList, JSON_PRETTY_PRINT);
}); 

$app->get('/friendsSorted', function (){
    $friendsList = DB::query("SELECT * FROM friends ORDER BY name DESC");
    echo json_encode($friendsList, JSON_PRETTY_PRINT);
}); 

$app->get('/friendsName', function (){
    $friendsList = DB::query("SELECT name FROM friends ORDER BY name");
    echo json_encode($friendsList, JSON_PRETTY_PRINT);
}); 


$app->get('/friends/:id', function($id) use ($app) {
    $friend = DB::queryFirstRow("SELECT * FROM friends WHERE id=%i", $id);
    if ($friend) {
        echo json_encode($friend, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

$app->post('/friends', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify friend data is valid
    DB::insert('friends', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
  
});

$app->put('/friends/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify friend data is valid
    // FIXME: fail with 400 if record does not exist
    DB::update('friends', $data, 'id=%d', $id);
    echo json_encode(true);
});

$app->delete('/friends/:id', function($id) {
    DB::delete('friends', 'id=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});

$app->run();