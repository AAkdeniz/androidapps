<?php

require_once 'vendor/autoload.php';

DB::$dbName = 'friendsdb';
DB::$user = 'friendsdb';
DB::$name = "sy4wGr7TIpmVrqVX";
DB::$encoding = 'utf8';
DB::$host = '127.0.0.1';
DB::$port =3333;
 
$app = new \Slim\Slim();

$app->get('/hello',function (){
echo 'Hello from PHP Slim';
});
$app->get('/hello/:name', function ($name){
echo "Hello $name from PHP Slim";
});
$app->get('/register/:name/:age', function ($name, $age){
DB::insert('friends', array('name' => $name, 'age' => $age));
echo "Record inserted with id=" . DB::insertId();       
});

$app->get('/list', function() {
    $friendsList = DB::query("SELECT * FROM friends");
    echo'<pre>';
    print_r($friendsList);
});

$app->run();