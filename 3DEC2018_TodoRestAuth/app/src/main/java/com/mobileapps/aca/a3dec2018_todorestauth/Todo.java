package com.mobileapps.aca.a3dec2018_todorestauth;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Todo {

    public Todo(int id, String task, Date dueDate, TaskStatus isDone) {
        this.id = id;
        this.task = task;
        this.dueDate = dueDate;
        this.isDone = isDone;
    }

    int id;
    String task;
    Date dueDate;
    TaskStatus isDone;

    enum TaskStatus {Pending, Done}

    @Override
    public String toString() {

        String dueDateLocal = Globals.dateFormatLocal.format(dueDate);
        return String.format("%d: %s due on %s (%s)", id, task, dueDateLocal, isDone);
    }

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}
