package com.mobileapps.aca.a3dec2018_todorestauth;

public class AuthenticationFailedException extends Exception {
    public AuthenticationFailedException(String msg) {
        super(msg);
    }
}
