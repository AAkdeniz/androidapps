package com.mobileapps.aca.a3dec2018_todorestauth;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText etEmail, etPassword;
    Button btLogin;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        etEmail =(EditText) findViewById(R.id.etEmail);
        etPassword =(EditText) findViewById(R.id.etPassword);
        btLogin =(Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Globals.PREF_EMAIL, email);
                editor.putString(Globals.PREF_PASSWORD, password);
                editor.commit();
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        etEmail.setText(sharedPreferences.getString(Globals.PREF_EMAIL,""));
        etPassword.setText(sharedPreferences.getString(Globals.PREF_PASSWORD,""));
    }
}
