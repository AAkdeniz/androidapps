package com.mobileapps.aca.a3dec2018_todorestauth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    ArrayList<Todo> todoList = new ArrayList<>();
    ArrayAdapter<Todo> todoAdapter;

    ListView lvTodos;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lvTodos = (ListView) findViewById(R.id.lvTodos);
        todoAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, todoList);
        lvTodos.setAdapter(todoAdapter);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Globals.dateFormatLocal = android.text.format.DateFormat.getDateFormat(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadTodosListAsyncTask().execute();
    }

    class LoadTodosListAsyncTask extends AsyncTask<Void, Void, Object> {

        public static final String TAG = "LoadTodosListAsyncTas";

        @Override
        protected Object doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /todos");
                ArrayList<Todo> result = new ArrayList<>();
                URL url = new URL(Globals.BASE_API_URL + "/todos");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                // authentication
                String username = sharedPreferences.getString(Globals.PREF_EMAIL, "");
                String password = sharedPreferences.getString(Globals.PREF_PASSWORD, "");
                String usernamepassword = username+":"+password;
                String authString = Base64.encodeToString(usernamepassword.getBytes(),Base64.DEFAULT);
                connection.setRequestProperty("Authorization", "Basic "+authString);
                //

                int httpCode = connection.getResponseCode();
                if (httpCode == 401) {
                    throw new AuthenticationFailedException("GET /todos failed with 401 http code (authorization required or invalid)");
                }
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                input = new InputStreamReader(connection.getInputStream());
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();
                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();
                    // userId
                    key = reader.nextName();
                    if (!key.equals("userId"))
                        throw new ParseException("userId expected when parsing", 0);
                    int userId = reader.nextInt();
                    // task
                    key = reader.nextName();
                    if (!key.equals("task"))
                        throw new ParseException("task expected when parsing", 0);
                    String task = reader.nextString();
                    // dueDate
                    key = reader.nextName();
                    if (!key.equals("dueDate"))
                        throw new ParseException("dueDate expected when parsing", 0);
                    String dueDateStr = reader.nextString();
                    Date dueDate = Todo.dateFormat.parse(dueDateStr);
                    // isDone
                    key = reader.nextName();
                    if (!key.equals("isDone"))
                        throw new ParseException("isDone expected when parsing", 0);
                    String isDoneStr = reader.nextString();
                    Todo.TaskStatus isDone = Todo.TaskStatus.valueOf(isDoneStr);

                    //
                    reader.endObject();
                    Todo Todo = new Todo(id, task, dueDate, isDone);
                    result.add(Todo);
                    Log.v(TAG, "Todo parsed and added to list: " + todoList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException | AuthenticationFailedException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return ex;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (result instanceof ArrayList) {
                ArrayList<Todo> list = (ArrayList<Todo>) result;
                todoList.clear();
                for (Todo Todo : list) {
                    todoList.add(Todo);
                }

                todoAdapter.notifyDataSetChanged();
            } else if (result instanceof AuthenticationFailedException) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            } else if (result instanceof Exception) {
                Toast.makeText(MainActivity.this, "API error fetching data", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "API error fetching data(Internal)", Toast.LENGTH_LONG).show();
                Log.wtf(TAG, "Unknown resukt type in onPostExecute");
            }
        }
    }
}
