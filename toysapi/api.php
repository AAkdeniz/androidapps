<?php

require_once 'vendor/autoload.php';

DB::$dbName = 'toysapi';
DB::$user = 'root';
DB::$password = "";
DB::$encoding = 'utf8';
DB::$host = '127.0.0.1'; // 'localhost' works on windows ?
DB::$port = 3306; 

$app = new \Slim\Slim();

// this script will always return JSON to any client
$app->response()->header('content-type', 'application/json');

$app->get('/toys', function() {
    $toysList = DB::query("SELECT * FROM toys");
    echo json_encode($toysList, JSON_PRETTY_PRINT);
});

$app->get('/toys/:id', function($id) use ($app) {
    $toy = DB::queryFirstRow("SELECT * FROM toys WHERE id=%i", $id);
    if ($toy) {
        echo json_encode($toy, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

$app->post('/toys', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    DB::insert('toys', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});

$app->put('/toys/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    // FIXME: fail with 400 if record does not exist
    DB::update('toys', $data, 'id=%d', $id);
    echo json_encode(true);
});

$app->delete('/toys/:id', function($id) {
    DB::delete('toys', 'id=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});

$app->run();

