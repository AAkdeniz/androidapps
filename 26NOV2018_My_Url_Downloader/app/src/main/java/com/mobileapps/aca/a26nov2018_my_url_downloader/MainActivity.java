package com.mobileapps.aca.a26nov2018_my_url_downloader;


import android.content.SharedPreferences;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText etUrl;
    ProgressBar pbDownloadProgress;
    TextView tvResult;
    Button btStartCancel;
    SharedPreferences sharedPreferences;
    public final static String PREF_URL = "url";
    DownloaderAsyncTask downloaderAsyncTask = null;
    private int progressBarStatus = 0;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        etUrl = (EditText) findViewById(R.id.etUrl);
        pbDownloadProgress = (ProgressBar) findViewById(R.id.pbDownloadProgress);
        tvResult = (TextView) findViewById(R.id.tvResult);
        btStartCancel = (Button) findViewById(R.id.btStartCancel);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // save URL from EditText to preferences
        String url = etUrl.getText().toString();
        sharedPreferences.edit().putString(PREF_URL, url).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // load URL from preferences into EditText
        String url = sharedPreferences.getString(PREF_URL, "");
        etUrl.setText(url);
    }

    public void onStartCancelClick(View v) {
        if (downloaderAsyncTask == null) { // start
            try {
                String urlStr = etUrl.getText().toString();
                URL url = new URL(urlStr); // may fail with exception
                downloaderAsyncTask = new DownloaderAsyncTask();
                downloaderAsyncTask.execute(url);
            } catch (MalformedURLException ex) {
                Toast.makeText(this, "URL invalid", Toast.LENGTH_LONG).show();
            }
        } else { // cancel
            downloaderAsyncTask.cancel(true);
        }
    }

    class DownloaderAsyncTask extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urlArray) {
            URL url = urlArray[0];

            StringBuilder result = new StringBuilder();
            byte dataBuffer[] = new byte[1024];
            try {
                BufferedInputStream inputStream = new BufferedInputStream(url.openStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            while (!isCancelled()) {
                // keep downloading chunks of file from the URL
                publishProgress(); // 0-100 percentage of downloaded file
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                }
            }
            return result.toString();
        }

        @Override
        public void onPreExecute() {
            // save URL from EditText to preferences
            String url = etUrl.getText().toString();
            sharedPreferences.edit().putString(PREF_URL, url).commit();
            // change button to "cancel download"
            btStartCancel.setText("Cancel Download");
        }

        @Override
        public void onProgressUpdate(Integer... progValuesArray) {
            int progress = progValuesArray[0];
            // set progress bar to show progress
            new Thread(new Runnable() {
                public void run() {
                    while (progressBarStatus < 100) {
                        progressBarStatus += 1;
                        // Update the progress bar and display the
                        //current value in the text view
                        handler.post(new Runnable() {
                            public void run() {
                                pbDownloadProgress.setProgress(progressBarStatus);
                                tvResult.setText(progressBarStatus+"/"+pbDownloadProgress.getMax());
                            }
                        });
                        try {
                            // Sleep for 200 milliseconds.
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }



        @Override
        public void onCancelled() {
            // change button back to "start download"
            // set progress bar to 0
            // set downloaderAsyncTask back to null
        }

        @Override
        public void onPostExecute(String result) {
            // put result into tvResult
            // change button back to "start download"
            // set progress bar to 0
            // set downloaderAsyncTask back to null
        }

    }

}