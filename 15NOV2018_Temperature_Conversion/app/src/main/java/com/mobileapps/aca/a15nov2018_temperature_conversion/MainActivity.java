package com.mobileapps.aca.a15nov2018_temperature_conversion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.temperature_conversion.MESSAGE";

    EditText etFromTemp;
    RadioGroup rgFrom, rgTo;
    TextView tvTo;
    RadioButton radioButtonFrom, radioButtonTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rgFrom = (RadioGroup) findViewById(R.id.rgFrom);
        rgTo = (RadioGroup) findViewById(R.id.rgTo);
        etFromTemp = (EditText) findViewById(R.id.etFromTemp);
        tvTo = (TextView) findViewById(R.id.tvTo);

    }

    public void convertTemperature(View v) {

        double value = Double.parseDouble(etFromTemp.getText().toString());

        double valueInCel = 0;
        switch (rgFrom.getCheckedRadioButtonId()) {

            case R.id.rbFromCelsius:
                valueInCel = value;
                break;
            case R.id.rbFromFahrenheit:
                valueInCel = (value - 32) * 5 / 9;
                break;
            case R.id.rbFromKelvin:
                valueInCel = value - 273.15;
                break;
            default:
                // Log.wtf("") FIXME
        }

        double valResult = 0;
        switch (rgTo.getCheckedRadioButtonId()) {

            case R.id.rbToCelsius:
                valResult = valueInCel;
                break;
            case R.id.rbToFahrenheit:
                valResult = valueInCel * 1.8 + 32;
                break;
            case R.id.rbToKelvin:
                valResult = valueInCel + 273.15;
                break;
            default:
                // Log.wtf("") FIXME
        }

        tvTo.setText(String.format("%.2f", valResult));
    }


    public void sendMessage(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        String message = tvTo.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

        // get selected radio button from radioGroup
        int selectedId1 = rgFrom.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioButtonFrom = (RadioButton) findViewById(selectedId1);

        // get selected radio button from radioGroup
        int selectedId2 = rgTo.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioButtonTo = (RadioButton) findViewById(selectedId2);


        String messageToast ="From "+ etFromTemp.getText().toString() + " " + radioButtonFrom.getText().toString() + " to "
                + radioButtonTo.getText().toString() + " result is " + tvTo.getText().toString();

        Toast toast = Toast.makeText(getApplicationContext(),
                messageToast,
                Toast.LENGTH_SHORT);

        toast.show();


    }
}
