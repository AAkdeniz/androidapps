package com.mobileapps.aca.a19nov2018_notepad;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class NotepadActivity extends AppCompatActivity {

    public static final String TAG = "NotepadActivity";

    EditText etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);
        etText = (EditText) findViewById(R.id.etText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notepad_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FileInputStream fileInputStream = null;
        Scanner fileInput = null;

        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            fileInput.useDelimiter("\\Z");
            //bug 1
            if (!fileInput.hasNext()) {
                return;
            }
            String text = fileInput.next();
        } catch (IOException e) {
            Toast.makeText(this, "Error reading text from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    public static final String FILE_NAME = "data.txt";

    @Override
    public void onStop() {
        super.onStop();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String text = etText.getText().toString();
            fileOutputStream.write(text.getBytes());
            Log.d(TAG, "File contents written");
        } catch (IOException e) {
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
