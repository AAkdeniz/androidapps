package com.mobileapps.aca.a20nov2018_friends_list;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    ListView lvFriends;
    ArrayList<String> friendsList;
    ArrayAdapter<String> friendsAdapter;

    public static final String FILE_NAME = "friends.txt";
    public static final String TAG = "Main Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        lvFriends = (ListView) findViewById(R.id.lvFriends);

        friendsList = new ArrayList<String>();

        friendsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, friendsList);
        lvFriends.setAdapter(friendsAdapter);

        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nameClicked = (String) adapterView.getItemAtPosition(i);
                showDeleteFriendDialog(nameClicked);
                return true;
            }
        });

        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nameClicked = (String) adapterView.getItemAtPosition(i);
                showAddEditFriendDialog(nameClicked);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:
                showAddEditFriendDialog(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDeleteFriendDialog(final String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Friend");
        builder.setMessage("Are you sure you want to delete \n" + name);
        //setup the additional edit text

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                friendsList.remove(name);
                friendsAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, name + " is deleted", Toast.LENGTH_LONG).show();
                Log.d("Main Activity", "Friend deleted: " + name);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private void showAddEditFriendDialog(final String editedName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(editedName == null ? "Add friend" : "Edit friend");
        // setup the additional edit text
        final EditText etFriendName = new EditText(this);
        etFriendName.setInputType(InputType.TYPE_CLASS_TEXT);
        etFriendName.setText(editedName == null ? "" : editedName);
        builder.setView(etFriendName);
        builder.setPositiveButton(editedName == null ? "Add" : "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newName = etFriendName.getText().toString();
                if (editedName == null) {
                    friendsList.add(newName);
                } else {
                    int index = friendsList.indexOf(editedName);
                    friendsList.set(index, newName);
                }
                friendsAdapter.notifyDataSetChanged();
                String action = editedName == null ? "added" : "updated";
                Toast.makeText(MainActivity.this, "Friend " + action, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Friend "+action+": " + newName);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadFriendsFromFile();
    }

    @Override
    public void onStop() {
        super.onStop();
        saveFriendsToFile();
    }

    private void loadFriendsFromFile() {
        FileInputStream fileInputStream = null;
        Scanner scanner = null;
        try {
            fileInputStream = openFileInput(FILE_NAME);
            scanner = new Scanner(fileInputStream);
            friendsList.clear();
            while (scanner.hasNextLine()) {
                String name = scanner.nextLine();
                friendsList.add(name);
            }
            friendsAdapter.notifyDataSetChanged();
            Toast.makeText(this, "Data loaded from file", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException ex) {
            // ignore if file does not exist yet
            Log.d(TAG, "Data file not found, probably first use");
        } catch (NoSuchElementException | IllegalStateException ex) {
            Log.e(TAG, "Error loading data from file", ex); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error loading data from file", Toast.LENGTH_LONG).show();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }
    private void saveFriendsToFile() {
        FileOutputStream fileOutputStream = null;
        PrintWriter printWriter = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            printWriter = new PrintWriter(fileOutputStream);

            for (String name : friendsList) {
                printWriter.println(name);
            }
            Log.d(TAG, "File contents written");
        } catch (IOException e) {
            Log.d(TAG, "Error saving data to file", e);    // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (printWriter != null) {
                printWriter.close();

            }
        }
    }
}
