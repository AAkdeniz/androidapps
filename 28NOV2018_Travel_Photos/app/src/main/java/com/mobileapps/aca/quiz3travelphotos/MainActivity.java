package com.mobileapps.aca.quiz3travelphotos;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    public static ArrayList<Trip> tripArrayList = new ArrayList<>();
    public static ArrayAdapter<Trip> tripArrayAdapter;
    private static final String TAG = "MainActivity";

    private static final String DATABASE_NAME = "trips_db";
    public static final String EXTRA_INDEX = "index";

    private AppDatabase appDatabase;

    ListView lvTrips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
        //
        lvTrips = (ListView) findViewById(R.id.lvTrips);

        tripArrayAdapter = new TripArrayAdapter(this,tripArrayList);
        lvTrips.setAdapter(tripArrayAdapter);


        lvTrips.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ItemView was clicked, find out which String (name) it was displaying
                Trip friendClicked = (Trip) parent.getItemAtPosition(position);
                int index = tripArrayList.indexOf(friendClicked);
                Intent intent = new Intent(MainActivity.this, AddTripActivity.class);
                intent.putExtra(EXTRA_INDEX, index);
                startActivity(intent);
            }
        });

        onLongClickForDelete();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.more_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_addTrip: {
                Intent intent = new Intent(this, AddTripActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.mi_photoFromURL: {
                Intent intent = new Intent(this, PhotoFromURLActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.mi_sortByDestination: {

                return true;
            }
            case R.id.mi_sortByDate: {

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onLongClickForDelete() {
        lvTrips.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, int i, long l) {

                Trip tripClicked = (Trip) adapterView.getItemAtPosition(i);
                showDeleteTripDialog(tripClicked);
                return true;
            }
        });
    }

    private void deleteTripDbAsyncTask(Trip trip) {
        new DeleteFriendDbAsyncTask().execute(trip);
        tripArrayAdapter.notifyDataSetChanged();
        Toast.makeText(MainActivity.this, trip.getDestination() + " is deleted!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Trip deleted: " + trip.getDestination());
    }



    private void showDeleteTripDialog(final Trip trip) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Trip");
        builder.setMessage("Are you sure you want to delete \n" + trip.getDestination());

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                try {
                    deleteTripDbAsyncTask(trip);
                } catch (Exception ex) {
                    Log.e(TAG, "DaoAccess.deleteTrip() failed", ex);

                }

                Toast.makeText(MainActivity.this,  trip.getDestination() + " is deleted", Toast.LENGTH_LONG).show();
                Log.d("Main Activity", "Trip deleted: " +  trip.getDestination());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    class DeleteFriendDbAsyncTask extends AsyncTask<Trip, Void, Void> {

        @Override
        protected Void doInBackground(Trip... params) {
            Trip trip = params[0];
            try {
                appDatabase.daoAccess().deleteTrip(trip);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.deleteTrip() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadTripFromDbAsyncTask().execute(); // reload the list
        }
    }


    class LoadTripFromDbAsyncTask extends AsyncTask<Void, Void, List<Trip>> {
        private static final String TAG = "LoadTripsDbAsyncTask";

        @Override
        protected List<Trip> doInBackground(Void... params) {
            try {
                List<Trip> list = appDatabase.daoAccess().fetchAllTrips();
                return list;

            } catch (Exception ex) {
                Log.e(TAG, "Database error when loading trip list", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Trip> list) {
            if (list == null) {
                Toast.makeText(MainActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                tripArrayList.clear();
                for (Trip trip : list) {
                    tripArrayList.add(trip);
                }
                tripArrayAdapter.notifyDataSetChanged();
            }
        }
    }

    public class TripArrayAdapter extends ArrayAdapter<Trip> {

        private static final String TAG = "TripArrayAdapter";

        private Context context;
        private ArrayList<Trip> list;

        public TripArrayAdapter(Context context, ArrayList<Trip> list) {
            super(context, R.layout.trip_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.trip_item, parent, false);

            TextView tvDestination = (TextView) rowView.findViewById(R.id.tvDestination);
            TextView tvDate = (TextView) rowView.findViewById(R.id.tvDate);
            ImageView ivPhoto = (ImageView) rowView.findViewById(R.id.ivPhoto);
            //
            Trip trip = list.get(position);
            tvDestination.setText(trip.destination);
            tvDate.setText(trip.date);

            Log.d(TAG, "getView: filepath : "+ trip.getPhotoUri());
            if (trip.getPhotoUri() != null && !trip.getPhotoUri().equals(" ")) {
                Uri bitmap = Uri.fromFile(new File(trip.getPhotoUri()));
                ivPhoto.setImageURI(bitmap);
            }

            return rowView;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new LoadTripFromDbAsyncTask().execute();
    }

//    private void sortTripByDestinationAsyncTask(Trip trip) {
//        new SortTripByDestinationAsyncTask().execute(trip.getDestination());
//        tripArrayAdapter.notifyDataSetChanged();
//        Toast.makeText(MainActivity.this, trip.getDestination() + " is deleted!", Toast.LENGTH_LONG).show();
//        Log.d("Main Activity", "Trip deleted: " + trip.getDestination());
//    }
//
//
//    abstract class SortTripByDestinationAsyncTask extends AsyncTask<List<Trip>, Void, Void> {
//
//        @Override
//        protected List<Trip> doInBackground(Trip... params) {
//            Trip trip = params[0];
//            try {
//                appDatabase.daoAccess().sortTripByDestination(trip.destination);
//
//            } catch (Exception ex) {
//                Log.e(TAG, "DaoAccess.deleteTrip() failed", ex);
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            new LoadTripFromDbAsyncTask().execute(); // reload the list
//        }
//    }
}




