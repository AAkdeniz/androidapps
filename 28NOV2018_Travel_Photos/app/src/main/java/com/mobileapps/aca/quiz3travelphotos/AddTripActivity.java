package com.mobileapps.aca.quiz3travelphotos;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class AddTripActivity extends AppCompatActivity {

    ImageView ivPhoto;
    EditText etDestination;
    EditText etDate;
    String imageFilePath;

    private static final String DATABASE_NAME = "friends_db";
    private static final String IMAGE_DIRECTORY = "/sdcard/Download";
    private static final String TAG = "AddEditActivity";
    private int GALLERY = 1;

    AppDatabase appDatabase;
    Trip currentTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();

        init();

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });


        Intent intent = getIntent();
        int index = intent.getIntExtra(MainActivity.EXTRA_INDEX, -1);
        currentTrip = (index == -1) ? null : MainActivity.tripArrayList.get(index);
        if (currentTrip != null) {
            etDestination.setText(currentTrip.destination);
            etDate.setText(currentTrip.date);
            ivPhoto.setImageResource(R.drawable.ic_launcher_foreground);


            // FIXME: setting photo does not work
        }


    }


    public void init() {
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        etDestination = (EditText) findViewById(R.id.etDestination);
        etDate = (EditText) findViewById(R.id.etDate);

    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        choosePhotoFromGallery();
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(AddTripActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    ivPhoto.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AddTripActivity.this, "Image saving failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        // TODO: If editing, load edit_menu instead
        menuInflater.inflate(currentTrip == null ? R.menu.add_menu : R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                addTrip();
                return true;
            }
            case R.id.mi_edit: {
                currentTrip.destination = etDestination.getText().toString();
                currentTrip.date = etDate.getText().toString();
                currentTrip.photoUri = imageFilePath;

                updateTripDbAsyncTask(currentTrip);
                finish();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addTrip(){
        String destination = etDestination.getText().toString();
        String date = etDate.getText().toString();

        String photoUri =imageFilePath;

        Trip trip = new Trip(0, destination,date, photoUri);
        new InsertTripDbAsyncTask().execute(new Trip(0, destination,date, photoUri));
        MainActivity.tripArrayList.add(trip);

        // FIXME: MainActivity needs to refresh list in onStart();
        MainActivity.tripArrayAdapter.notifyDataSetChanged();
        finish();

        }


    class InsertTripDbAsyncTask extends AsyncTask<Trip, Void, Void> {

        @Override
        protected Void doInBackground(Trip... params) {
            Trip trip = params[0];
            try {
                appDatabase.daoAccess().insertTrip(trip);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.insertTrip() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
                new LoadTripsFromDbAsyncTask().execute(); // reload the list
                Toast.makeText(AddTripActivity.this, "Trip added", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddTripActivity.this, "Database error when adding Trip", Toast.LENGTH_LONG).show();
            }
        }
    }

    class LoadTripsFromDbAsyncTask extends AsyncTask<Void, Void, List<Trip>> {

        private static final String TAG = "LoadTripsDbAsyncTask";

        @Override
        protected List<Trip> doInBackground(Void... params) {
            try {
                List<Trip> list = appDatabase.daoAccess().fetchAllTrips();
                return list;

            } catch (Exception ex) {
                Log.e(TAG, "Database error when loading trip list", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Trip> list) {
            if (list == null) {
                Toast.makeText(AddTripActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                MainActivity.tripArrayList.clear();
                for (Trip trip : list) {
                    MainActivity.tripArrayList.add(trip);
                }
                MainActivity.tripArrayAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new LoadTripsFromDbAsyncTask().execute();
    }

    private void updateTripDbAsyncTask(Trip trip) {
        new UpdateFriendDbAsyncTask().execute(trip);
        MainActivity.tripArrayAdapter.notifyDataSetChanged();
        Toast.makeText(AddTripActivity.this, trip.getDestination()+ " is updated!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Friend updated: " + trip.getDestination());
    }


    class UpdateFriendDbAsyncTask extends AsyncTask<Trip, Void, Void> {

        @Override
        protected Void doInBackground(Trip... params) {
            Trip trip = params[0];
            try {
                appDatabase.daoAccess().updateTrip(trip);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.updateFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadTripsFromDbAsyncTask().execute(); // reload the list
        }

    }

}
