package com.mobileapps.aca.quiz3travelphotos;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.UrlQuerySanitizer;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class PhotoFromURLActivity extends AppCompatActivity {

    public static final String EXTRA_BMP = "bmp";
    public static final String TAG = "MainActivity";

    // FIXME: Instead of this you should save to a file !!!!!!
    // public static byte[] bytesOfImage;

    EditText etUrl;
    Button btDownload;
    ImageView ivPhoto;
    Bitmap bmp;


    SharedPreferences sharedPreferences;
    public final static String PREF_URL = "url";
    DownloaderAsyncTask downloaderAsyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_from_url);
        //
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btDownload = (Button) findViewById(R.id.btDownload);

        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        Intent intent = getIntent();
        byte[] result = intent.getByteArrayExtra(EXTRA_BMP);
//         byte[] result = MainActivity.bytesOfImage;
        bmp = BitmapFactory.decodeByteArray(result, 0, result.length);
        ivPhoto.setImageBitmap(bmp);

    }


    @Override
    protected void onStop() {
        super.onStop();
        // save URL from EditText to preferences
        String url = etUrl.getText().toString();
        sharedPreferences.edit().putString(PREF_URL, url).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // load URL from preferences into EditText
        String url = sharedPreferences.getString(PREF_URL, "");
        etUrl.setText(url);
    }


    class DownloaderAsyncTask extends AsyncTask<URL, Integer, byte[]> {

        public final static String TAG = "DownloaderAsyncTask";

        @Override
        protected byte[] doInBackground(URL... urlArray) {
            URL url = urlArray[0];
            BufferedInputStream bufferedInputStream = null;
            // StringBuilder result = new StringBuilder();
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            try {
                byte dataBuffer[] = new byte[1024];
                int totalBytesRead = 0, contentSize, bytesRead;

                URLConnection conn = url.openConnection();
                contentSize = conn.getContentLength();
                String contentType = conn.getContentType();
                Log.d(TAG, "Content type is: " + contentType);
                Log.d(TAG, "Content size is: " + contentSize);

                bufferedInputStream = new BufferedInputStream(conn.getInputStream());

                while ((bytesRead = bufferedInputStream.read(dataBuffer)) != -1) {
                    if (isCancelled()) {
                        break;
                    }
                    //result.append(new String(dataBuffer));
                    result.write(dataBuffer, 0, bytesRead);
                    //
                    totalBytesRead += bytesRead;
                    if (contentSize != -1) {
                        int progressPercentage = 100 * totalBytesRead / contentSize;
                        publishProgress(progressPercentage);

                        try {
                            int delay = 4000 / (contentSize / 1024);
                            Thread.sleep(delay);
                        } catch (InterruptedException ex) {
                        }
                    } else {
                        publishProgress(totalBytesRead % 100);
                    }
                }
            } catch (IOException ex) {
                Log.e(TAG, "Exception while reading from URL" + url, ex);
                return null;
            } finally {
                if (bufferedInputStream != null) {
                    try {
                        bufferedInputStream.close();
                    } catch (IOException ex) {
                        Log.e(TAG, "Exception while closing BufferedInputStream", ex);
                    }
                }
            }

            return result.toByteArray();
        }

        @Override
        public void onPreExecute() {

            String url = etUrl.getText().toString();
            sharedPreferences.edit().putString(PREF_URL, url).commit();

        }

        @Override
        public void onProgressUpdate(Integer... progValuesArray) {
            int progress = progValuesArray[0];
            Log.d(TAG, "Progress: " + progress);

        }

        @Override
        public void onCancelled() {
            Log.d(TAG, "Task cancelled");

        }

        @Override
        public void onPostExecute(byte[] result) {
            if (result == null) {
                Log.w(TAG, "Result is null, something went wrong with downloading");
                return;
            }
            // is it an image?
            Bitmap bmp = BitmapFactory.decodeByteArray(result, 0, result.length);
            if (bmp != null) {

                Intent intent = new Intent(PhotoFromURLActivity.this, PhotoFromURLActivity.class);
                intent.putExtra(EXTRA_BMP, result);
                startActivity(intent);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.image_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_save_to_gallery: {
                MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "title", "desc");
                Toast.makeText(this, "Image saved", Toast.LENGTH_LONG).show();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}