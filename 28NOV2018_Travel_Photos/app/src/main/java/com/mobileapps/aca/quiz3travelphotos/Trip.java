package com.mobileapps.aca.quiz3travelphotos;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "Trips")
public class Trip {

    @PrimaryKey(autoGenerate = true)
    private int id;

    String destination;
    String date;
    String photoUri;

    public Trip(int id, String destination, String date, String photoUri) {
        this.id = id;
        this.destination = destination;
        this.date = date;
        this.photoUri = photoUri;
    }

    @Override
    public String toString() {
        return String.format("%d: %s: %s: %s: ", id, destination, date, photoUri);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return id == trip.id &&
                Objects.equals(destination, trip.destination) &&
                Objects.equals(date, trip.date) &&
                Objects.equals(photoUri, trip.photoUri);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, destination, date, photoUri);
    }
}