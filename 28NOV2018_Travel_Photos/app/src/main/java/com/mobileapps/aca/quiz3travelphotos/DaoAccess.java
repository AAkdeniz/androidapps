package com.mobileapps.aca.quiz3travelphotos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertTrip(Trip trip);

    @Query("SELECT * FROM Trips ORDER BY date = :date")
    Trip sortTripByDate(String date);

    @Query("SELECT * FROM Trips ORDER BY destination= :destination")
    Trip sortTripByDestination(String destination);

    @Query("SELECT * FROM Trips")
    List<Trip> fetchAllTrips();

    @Update
    void updateTrip(Trip trip);

    @Delete
    void deleteTrip(Trip trip);


}