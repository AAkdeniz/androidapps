package com.mobileapps.aca.quiz1sandwichorder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.Quiz1SandwichOrder.MESSAGE";

    EditText etName;
    RadioGroup rgBreadType;
    CheckBox cbToasted;
    RadioButton rbSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = (EditText) findViewById(R.id.etName);
        rgBreadType = (RadioGroup) findViewById(R.id.rgBreadType);
        cbToasted = (CheckBox) findViewById(R.id.cbToasted);
    }

    public void onOrderClick(View v) {

        // get selected radio button from radioGroup
        int selectedId = rgBreadType.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        rbSelected = (RadioButton) findViewById(selectedId);
        String message = "";
        String isChecked = "";

        if (cbToasted.isChecked()) {
            isChecked = "toasted";
        } else isChecked = "not toasted";


        if (v.getId() == R.id.btOderHere) {
            message = etName.getText().toString() + " ordered sandwich with " + rbSelected.getText().toString() + " bread, " + isChecked + ".";
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
        if (v.getId() == R.id.btOrderToGo) {
            Intent intent = new Intent(this, ResultActivity.class);
            message = etName.getText().toString() + " ordered sandwich with " + rbSelected.getText().toString() + " bread, " + isChecked + ".";
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);

        }
    }
}
