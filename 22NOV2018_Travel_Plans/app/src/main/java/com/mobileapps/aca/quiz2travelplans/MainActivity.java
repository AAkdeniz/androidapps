package com.mobileapps.aca.quiz2travelplans;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    public static final String FILE_NAME = "data.txt";

    public static ArrayList<Trip> tripList = new ArrayList<>();

    ListView lvTrips;
    ArrayAdapter<Trip> tripAdapter;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvTrips = (ListView) findViewById(R.id.lvTrips);
        tripAdapter = new TripArrayAdapter(this, tripList);
        lvTrips.setAdapter(tripAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loadDataFromFile();
        tripAdapter.notifyDataSetChanged();
    }


    @Override
    public void onStop() {
        super.onStop();
        saveDataToFile();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        saveDataToFile();
    }

    private void loadDataFromFile() {
        FileInputStream fileInputStream = null;
        Scanner fileInput = null;
        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            tripList.clear();
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    // FIXME: verify data structure validity when reading file
                    String[] data = line.split(";");
                    String from = data[0];
                    String to = data[1];
                    Date dueDate = dateFormat.parse(data[2]); // may throw parse exception
                    String transport = data[3];
                    int budget = Integer.parseInt(data[4]);

                    Trip trip = new Trip(from, to, dueDate, transport, budget);
                    tripList.add(trip);
                } catch (ParseException ex) {
                    Log.e(TAG, "Error parsing data from file", ex); // NOTE: 3rd parameter is the Exception !
                    Toast.makeText(this, "Error parsing data from file", Toast.LENGTH_LONG).show();
                }
            }
            tripAdapter.notifyDataSetChanged();
            Toast.makeText(this, "Data loaded from file", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException ex) {
            // ignore if file does not exist yet
            Log.d(TAG, "Data file not found, probably first use");
        } catch (NoSuchElementException | IllegalStateException ex) {
            Log.e(TAG, "Error loading data from file", ex); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error loading data from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    private void saveDataToFile() {
        FileOutputStream fileOutputStream = null;
        PrintWriter printWriter = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            printWriter = new PrintWriter(fileOutputStream);
            for (Trip trip : tripList) {
                String dateStr = dateFormat.format(trip.date);
                printWriter.printf("%s;%s;%s;%s;%s\n", trip.from, trip.to, dateStr, trip.transport, trip.budget);
            }
            Log.d(TAG, "File contents written");
        } catch (IOException ex) {
            Log.e(TAG, "Error saving data to file", ex); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

    public class TripArrayAdapter extends ArrayAdapter<Trip> {

        private Context context;
        private ArrayList<Trip> list;


        public TripArrayAdapter(Context context, ArrayList<Trip> list) {
            super(context, R.layout.trip_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.trip_item, parent, false);
            TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
            ImageView ivIcon = (ImageView) rowView.findViewById(R.id.ivIcon);
            //
            Trip trip = list.get(position);

            tvFirstLine.setText("From " + trip.from + " to: " + trip.to);
            tvSecondLine.setText(trip.date.toString()); // FIXME: should format date
           // ivIcon.setImageResource(AddActivity.getMyImage());  FIXME: should image change when the transportation change

            return rowView;
        }
    }



}


