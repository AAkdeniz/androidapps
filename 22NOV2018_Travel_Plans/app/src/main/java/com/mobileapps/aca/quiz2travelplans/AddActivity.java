package com.mobileapps.aca.quiz2travelplans;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddActivity extends AppCompatActivity {
    EditText etFrom;
    EditText etTo;
    EditText etDate;
    static RadioGroup rgTransportation;
    TextView tvBudget;
    SeekBar seekBar;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initialize();

    }

    public void initialize() {
        etFrom = (EditText) findViewById(R.id.etFrom);
        etTo = (EditText) findViewById(R.id.etTo);
        etDate = (EditText) findViewById(R.id.etDate);
        rgTransportation = (RadioGroup) findViewById(R.id.rgTransportation);

        seekBar = findViewById(R.id.sbBudget);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        int progress = seekBar.getProgress();
        tvBudget = findViewById(R.id.tvBudget);
        tvBudget.setText("Budget: " + progress);


    }

    //FIXME: Use the method for changing the image after the transportation is selected

    public int getMyImage() {

        switch (rgTransportation.getCheckedRadioButtonId()) {

            case R.id.rbCar:
                return R.drawable.baseline_commute_black_36dp;
            case R.id.rbBus:
                return R.drawable.baseline_commute_black_36dp;
            case R.id.rbTrain:
                return R.drawable.baseline_train_black_36dp;
            case R.id.rbPlane:
                return R.drawable.baseline_airplanemode_active_black_36dp;
            default:
                return -1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                String from = etFrom.getText().toString();
                String to = etTo.getText().toString();

                Date date = null;
                try {
                    date = dateFormat.parse(etDate.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String transportation = selectTransportation();
                int budget = Integer.parseInt(tvBudget.getText().toString());

                Trip trip = new Trip(from, to, date, transportation, budget);

                MainActivity.tripList.add(trip);
                finish(); // dismissed the activity, pops it from the stack
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String selectTransportation() {

        String transportation = "";

        switch (rgTransportation.getCheckedRadioButtonId()) {

            case R.id.rbCar:
                return "Car";
            case R.id.rbBus:
                return "Bus";
            case R.id.rbTrain:
                return "Train";
            case R.id.rbPlane:
                return "Plane";
            default:
                return transportation;
        }
    }


    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            tvBudget.setText("Budget: " + progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // called when the user first touches the SeekBar
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // called after the user finishes moving the SeekBar
        }
    };
}

