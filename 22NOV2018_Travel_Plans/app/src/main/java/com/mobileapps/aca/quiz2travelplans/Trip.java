package com.mobileapps.aca.quiz2travelplans;

import java.util.Date;

public class Trip {

    public Trip(String from, String to, Date date, String transport, int budget) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.transport = transport;
        budget = budget;
    }

    String from;
    String to;
    Date date;
    String transport;
    int budget;

    @Override
    public String toString() {
        return "Trip{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", date=" + date +
                ", transport='" + transport + '\'' +
                ", budget=" + budget +
                '}';
    }
}
