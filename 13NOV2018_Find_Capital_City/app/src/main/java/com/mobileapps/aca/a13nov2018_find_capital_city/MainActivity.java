package com.mobileapps.aca.a13nov2018_find_capital_city;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CountryExpert expert = new CountryExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickFindCapital(View view) {
        //Get a reference to the TextView
        TextView countries = findViewById(R.id.capital);

        //Get a reference to the Spinner
        Spinner capital = findViewById(R.id.country_names);

        //Get the selected item in the Spinner
        String countryName = String.valueOf(capital.getSelectedItem());

        //Display the selected item

        List<String> countryList = expert.getCapitals(countryName);
        StringBuilder countryFormatted = new StringBuilder();
        for (String country : countryList) {

            countryFormatted.append(country).append("\n");
        }
        countries.setText(countryFormatted);
    }
}
