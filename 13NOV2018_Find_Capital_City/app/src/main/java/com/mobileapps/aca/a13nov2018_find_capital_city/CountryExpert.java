package com.mobileapps.aca.a13nov2018_find_capital_city;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class CountryExpert {
    List<String> getCapitals(String capital){
        List<String>countries = new ArrayList<>();
        if(capital.equals("Turkey")){
            countries.add("Ankara");
        }
        if(capital.equals("Canada")){
            countries.add("Ottowa");
        }
        if(capital.equals("France")){
            countries.add("Paris");
        }
        if(capital.equals("Germany")){
            countries.add("Berlin");
        }
        if(capital.equals("Greece")){
            countries.add("Athens");
        }
        if(capital.equals("Russia")){
            countries.add("Moscow");
        }
        if(capital.equals("Italy")){
            countries.add("Rome");
        }
        if(capital.equals("China")){
            countries.add("Beijing");
        }if(capital.equals("Iran")){
            countries.add("Tahran");
        }
        return countries;
    }
}
