package com.example.mobileapps.toysrestapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_TODO_ID = "id";


    public static final String BASE_API_URL = "http://10.0.2.2:8803/api.php" ;

    public static final String TAG = "MainActivity" ;
    ListView lvToys;

    ArrayList<Toy> toyList = new ArrayList<>() ;
    ArrayAdapter<Toy> toyArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvToys =  (ListView) findViewById(R.id.lvToys) ;
        toyArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, toyList) ;
        lvToys.setAdapter(toyArrayAdapter);

        //******************* On Long Click ******************//
        lvToys.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toy myToy = (Toy) parent.getItemAtPosition(position);
                new DeleteToyAsyncTask().execute (myToy);

                return true;
            }

        });
        //------------------------------------------------------------------//


        //******************* On Click ******************//
        lvToys.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int postionclicked = position;
                String nameClicked = (String) parent.getItemAtPosition(position).toString();               //
                int myid = Integer.parseInt(nameClicked.substring(0,1));
                Toy myToy =  (Toy) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                String fid = myToy.id+"";
                String fname = myToy.name+"";
                String ftype = myToy.type.toString();
                intent.putExtra("id",fid);
                intent.putExtra("name",fname);
                intent.putExtra("type",ftype);
                startActivity(intent);

            }

        });
        //------------------------------------------------------------------//
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadToysListAsyncTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent= new Intent(this , AddEditActivity.class) ;
                startActivity(intent);
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class LoadToysListAsyncTask extends AsyncTask<Void, Void, ArrayList<Toy>> {

        public static final String TAG = "LoadToysListAsyncTask" ;

        @Override
        protected ArrayList<Toy> doInBackground(Void... voids) {
            InputStreamReader input = null ;
            JsonReader reader = null ;
            try {
                ArrayList<Toy> result = new ArrayList<>() ;
                URL url = new URL(MainActivity.BASE_API_URL + "/toys") ;
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();
                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();
                    // name
                    key = reader.nextName();
                    if (!key.equals("name")) throw new ParseException("name expected when parsing", 0);
                    String name = reader.nextString();
                    // type
                    key = reader.nextName();
                    if (!key.equals("type"))
                        throw new ParseException("type expected when parsing", 0);
                    String typeStr = reader.nextString();
                    Toy.toyType toyType = Toy.toyType.valueOf(typeStr);
                    Toy toy = new Toy( id,  name, toyType);
                    reader.endObject();
                    result.add(toy);
                    Log.v(TAG, "Friend parsed and added to list: " + toyList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Toy> result) {
            toyList.clear();
            if (result == null ) {
                Toast.makeText(MainActivity.this, "API error fetching data", Toast.LENGTH_SHORT).show();
                return;
            }
            for (Toy toy : result) {
                toyList.add(toy);
            }
            toyArrayAdapter.notifyDataSetChanged();
        }
    }

    //******************************** Delete *********************************//
    class DeleteToyAsyncTask extends AsyncTask<Toy , Void, Boolean> {

        public static final String TAG = "DeleteToyAsyncTask";

        @Override
        protected Boolean doInBackground(Toy... params) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "DELETE /toys");
                ArrayList<Toy> result = new ArrayList<>();
                Toy selectedToy = params[0];
                int id = selectedToy.id;
                URL url = new URL(MainActivity.BASE_API_URL + "/toys/"+id);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("DELETE");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);

                return true;
            } catch (IOException  ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return false;
            }

        }

        @Override
        protected  void onPostExecute(Boolean result) {
            toyList.clear();
            if (result == false) {
                Toast.makeText(MainActivity.this, "API error deleting toy", Toast.LENGTH_LONG).show();
                return ;
            }else{
                new LoadToysListAsyncTask().execute();
                Toast.makeText(MainActivity.this, "Toy Deleted Successfully", Toast.LENGTH_LONG).show();
            }

        }
    }
}
