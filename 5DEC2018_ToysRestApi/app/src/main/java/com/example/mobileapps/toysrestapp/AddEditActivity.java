package com.example.mobileapps.toysrestapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddEditActivity extends AppCompatActivity {
 TextView tvId ;
 EditText etName;
 Spinner spType;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        tvId = (TextView)findViewById(R.id.tvId);
        etName= (EditText)findViewById(R.id.etName);
        spType= (Spinner)findViewById(R.id.spType);

        //create a list of items for the spinner.
        String[] items = new String[]{"plastic","large","chewy","mechanical"};
        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        //There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        //set the spinners adapter to the previously created one.
        spType.setAdapter(adapter);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {

        } else {

            tvId.setText(extras.getString("id"));
            etName.setText(extras.getString("name"));
             String spValu=(extras.getString("type"));
            int spinnerPosition = adapter.getPosition(spValu);
            spType.setSelection(spinnerPosition);

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        if (tvId.getText() == "") {
            menuInflater.inflate(R.menu.add_menu, menu);
        } else {
            menuInflater.inflate(R.menu.edit_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                // FIXME: verify inputs make sense
                String name = etName.getText().toString();
                String type = spType.getSelectedItem().toString();
                Toy.toyType myType  = Toy.toyType.valueOf(type);
                Toy myToy = new Toy(0, name, myType);
                new AddToyAsyncTask().execute(myToy);
                return true;
            }
            case R.id.mi_save: {
                // FIXME: verify inputs make sense
                int id = Integer.parseInt(tvId.getText().toString())  ;
                String name = etName.getText().toString();
                String type = spType.getSelectedItem().toString();
                Toy.toyType myType  = Toy.toyType.valueOf(type);
                Toy myToy = new Toy(id, name, myType);
                new UpdateToyAsyncTask().execute(myToy);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    class AddToyAsyncTask extends AsyncTask<Toy, Void, Boolean> {

        public static final String TAG = "AddToyAsyncTask";

        @Override
        protected Boolean doInBackground(Toy... paramsList) {
            Toy toy = paramsList[0];
            HttpURLConnection connection = null;
            try {
                Log.v(TAG, "POST /Toys " + toy.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/toys");
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                // String json = String.format("{ \"name\": \"%s\", \"age\": \"%d\" }", friend.name, friend.age);
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("name", toy.name);
                jsonData.put("type", toy.type);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close(); // FIXME - should be done in finally
                //
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }
            // FIXME: finally missing closing print writer, connection
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(AddEditActivity.this, "Toy added", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(AddEditActivity.this, "API error adding Toy", Toast.LENGTH_LONG).show();
            }
        }
    }

    //******************************** Update *********************************//
    class UpdateToyAsyncTask extends AsyncTask<Toy, Void, Boolean> {

        public static final String TAG = "UpdateToyAsyncTask";

        @Override
        protected Boolean doInBackground(Toy... paramsList) {
            Toy toy = paramsList[0];
            HttpURLConnection connection = null;
            int id = toy.id;
            try {
                Log.v(TAG, "Update /toys " + toy.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/toys/"+id);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("id", toy.id);
                jsonData.put("name", toy.name);
                jsonData.put("type", toy.type);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close();
                //
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }

        }

        @Override
        protected  void onPostExecute(Boolean result) {

            if (result == false) {
                Toast.makeText(AddEditActivity.this, "API error updating Toy", Toast.LENGTH_LONG).show();
                return ;
            }else{

                Toast.makeText(AddEditActivity.this, "Toy updated Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

        }
    }


}
