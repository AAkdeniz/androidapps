package com.example.mobileapps.toysrestapp;

public class Toy {
    int id ;
    String name ;
    toyType type;
    enum toyType {plastic,large,chewy,mechanical};

    public Toy() {
    }

    public Toy(int id, String name, toyType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("%d: -  %s (%s)", id, name, type);

    }
}
