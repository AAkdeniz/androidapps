package com.mobileapps.aca.a16nov2018_creating_contextual_menus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvOption = (TextView) findViewById(R.id.tvOptions);
        registerForContextMenu(tvOption);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose your level");

        getMenuInflater().inflate(R.menu.contextual_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.beginner:
                Toast.makeText(this, "Beginner clicked", Toast.LENGTH_LONG).show();
                return true;
            case R.id.intermediate:
                Toast.makeText(this, "Intermediate clicked", Toast.LENGTH_LONG).show();
                return true;
            case R.id.advanced:
                Toast.makeText(this, "Advanced clicked", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
