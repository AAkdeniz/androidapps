package com.mobileapps.aca.a16nov2018_create_popup_menu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showPopup(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.show();

    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.gameOfThrones:
                Toast.makeText(this, "Game of Thrones is going to start in a few minutes", Toast.LENGTH_LONG).show();
                return true;
            case R.id.braveHeart:
                Toast.makeText(this, "Brave Heart is going to start in a few minutes", Toast.LENGTH_LONG).show();
                return true;
            case R.id.theGodFather:
                Toast.makeText(this, "The God Father is going to start in a few minutes", Toast.LENGTH_LONG).show();
                return true;
            case R.id.sixthSense:
                Toast.makeText(this, "Sixth Sense is going to start in a few minutes", Toast.LENGTH_LONG).show();
                return true;
            default:
                return false;
        }
    }
}
