<?php

require_once 'vendor/autoload.php';

DB::$dbName = 'auctionsdb';
DB::$user = 'root';
DB::$password = "";
DB::$encoding = 'utf8';
DB::$host = '127.0.0.1'; // 'localhost' works on windows ?
DB::$port = 3306; 




$app = new \Slim\Slim();

// this script will always return JSON to any client
$app->response()->header('content-type', 'application/json');

$app->get('/users', function() {
    $userList = DB::query("SELECT * FROM users");
    echo json_encode($userList, JSON_PRETTY_PRINT);
});

$app->get('/users/:id', function($id) use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $id);
    if ($user) {
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

$app->post('/users', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    DB::insert('users', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});

$app->put('/users/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    // FIXME: fail with 400 if record does not exist
    DB::update('users', $data, 'id=%d', $id);
    echo json_encode(true);
});

$app->delete('/users/:id', function($id) {
    DB::delete('users', 'id=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});











//$app->get('/items', function() {
//    $itemList = DB::query("SELECT * FROM items");
//    echo json_encode($itemList, JSON_PRETTY_PRINT);
//});

$app->get('/items/:id', function($id) use ($app) {
    $item = DB::queryFirstRow("SELECT * FROM items WHERE id=%i", $id);
    if ($item) {
        echo json_encode($item, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

$app->post('/items', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    DB::insert('items', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});

$app->put('/items/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify toy data is valid
    // FIXME: fail with 400 if record does not exist
    DB::update('items', $data, 'id=%d', $id);
    echo json_encode(true);
});

$app->delete('/items/:id', function($id) {
    DB::delete('items', 'id=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});

$app->get('/items', function() use ($app) {
    $itemsList = DB::query("SELECT items.id, items.ownerId, users.email as ownerEmail, "
            . " items.description, items.highestBid, items.highestBidderEmail "
            . " FROM items,users WHERE items.ownerId = users.id ");
    echo json_encode($itemsList, JSON_PRETTY_PRINT);
});

$app->run();

