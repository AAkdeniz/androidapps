package com.mobileapps.aca.a21nov2018_my_todo;

import java.util.Date;

public class Todo {
    public Todo(String task, Date dueDate, boolean isDone) {
        this.task = task;
        this.dueDate = dueDate;
        this.isDone = isDone;
    }

    String task;
    Date dueDate;
    boolean isDone;

    @Override
    public String toString() {
        return "Todo{" +
                "task='" + task + '\'' +
                ", dueDate=" + dueDate +
                ", isDone=" + isDone +
                '}';
    }
}
