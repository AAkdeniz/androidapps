package com.mobileapps.aca.a21nov2018_my_todo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String EXTRA_INDEX = "index";
    public static final String FILE_NAME = "data.txt";

    public static ArrayList<Todo> todoList = new ArrayList<>();

    ListView lvTodos;
    ArrayAdapter<Todo> todoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // add test data
        todoList.add(new Todo("buy milk", new Date(), false));
        todoList.add(new Todo("go for a walk", new Date(), true));

        lvTodos = (ListView) findViewById(R.id.lvTodos);
        todoAdapter = new TodoArrayAdapter(this, todoList);
        lvTodos.setAdapter(todoAdapter);
        // TODO: click-to edit (homework)
        lvTodos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ItemView was clicked, find out which String (name) it was displaying
                Todo todoClicked = (Todo) parent.getItemAtPosition(position);
                int index = todoList.indexOf(todoClicked);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                intent.putExtra(EXTRA_INDEX, index);
                startActivity(intent);
            }
        });
        // use intent's Extra to pass integer Index of todoList item that is being edited
    }


    @Override
    public void onStart() {
        super.onStart();
        loadDataFromFile();
        todoAdapter.notifyDataSetChanged();
    }


    @Override
    public void onStop() {
        super.onStop();
        saveDataToFile();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        saveDataToFile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private void loadDataFromFile() {
        FileInputStream fileInputStream = null;
        Scanner fileInput = null;
        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            todoList.clear();
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    // FIXME: verify data structure validity when reading file
                    String[] data = line.split(";");
                    String task = data[0];
                    Date dueDate = dateFormat.parse(data[1]); // may throw parse exception
                    boolean isDone = data[2].equals("done");
                    Todo todo = new Todo(task, dueDate, isDone);
                    todoList.add(todo);
                } catch (ParseException ex) {
                    Log.e(TAG, "Error parsing data from file", ex); // NOTE: 3rd parameter is the Exception !
                    Toast.makeText(this, "Error parsing data from file", Toast.LENGTH_LONG).show();
                }
            }
            todoAdapter.notifyDataSetChanged();
            Toast.makeText(this, "Data loaded from file", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException ex) {
            // ignore if file does not exist yet
            Log.d(TAG, "Data file not found, probably first use");
        } catch (NoSuchElementException | IllegalStateException ex) {
            Log.e(TAG, "Error loading data from file", ex); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error loading data from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    private void saveDataToFile() {
        FileOutputStream fileOutputStream = null;
        PrintWriter printWriter = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            printWriter = new PrintWriter(fileOutputStream);
            for (Todo todo : todoList) {
                String dateStr = dateFormat.format(todo.dueDate);
                printWriter.printf("%s;%s;%s\n", todo.task, dateStr, todo.isDone ? "done" : "pending");
            }
            Log.d(TAG, "File contents written");
        } catch (IOException ex) {
            Log.e(TAG, "Error saving data to file", ex ); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

    public class TodoArrayAdapter extends ArrayAdapter<Todo> {

        private Context context;
        private ArrayList<Todo> list;

        public TodoArrayAdapter(Context context, ArrayList<Todo> list) {
            super(context, R.layout.todo_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.todo_item, parent, false);
            TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
            ImageView ivIcon = (ImageView) rowView.findViewById(R.id.ivIcon);
            //
            Todo todo = list.get(position);
            tvFirstLine.setText(todo.task);
            tvSecondLine.setText(todo.dueDate.toString()); // FIXME: should format date
            ivIcon.setImageResource(todo.isDone ? android.R.drawable.star_on : android.R.drawable.star_off);

            //
            return rowView;
        }
    }
}