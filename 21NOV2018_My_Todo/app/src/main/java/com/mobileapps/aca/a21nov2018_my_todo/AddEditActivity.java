package com.mobileapps.aca.a21nov2018_my_todo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Switch;

import java.util.Calendar;
import java.util.Date;

public class AddEditActivity extends AppCompatActivity {

    EditText etTask;
    CalendarView cvDueDate;
    Switch swIsDone;

    Todo currTodo; // non-null when editing

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        //
        etTask = (EditText) findViewById(R.id.etTask);
        cvDueDate = (CalendarView) findViewById(R.id.cvDueDate);
        swIsDone = (Switch) findViewById(R.id.swIsDone);
        // TODO: Check if Extra with Index integer value was provided
        // if yes - load Todo from MainActivity.todoList
        Intent intent = getIntent();
        int index = intent.getIntExtra(MainActivity.EXTRA_INDEX, -1);
        currTodo = (index == -1) ? null : MainActivity.todoList.get(index);
        if (currTodo != null) {
            etTask.setText(currTodo.task);
            // FIXME: setting date does not work
            cvDueDate.setDate(currTodo.dueDate.getTime());
            swIsDone.setChecked(currTodo.isDone);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        // TODO: If editing, load edit_menu instead
        menuInflater.inflate(currTodo == null ? R.menu.add_menu : R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                String task = etTask.getText().toString();
                Date dueDate = new Date(cvDueDate.getDate());
                boolean isDone = swIsDone.isChecked();
                Todo todo = new Todo(task, dueDate, isDone);
                MainActivity.todoList.add(todo);
                // FIXME: MainActivity needs to refresh list in onStart();
                finish(); // dismissed the activity, pops it from the stack
                return true;
            }
            case R.id.mi_save: {
                currTodo.task = etTask.getText().toString();
                currTodo.dueDate = new Date(cvDueDate.getDate());
                currTodo.isDone = swIsDone.isChecked();
                finish(); // dismissed the activity, pops it from the stack
                return true;
            }
            case R.id.mi_delete: {
                // TODO: display a ok/cancel dialog
                MainActivity.todoList.remove(currTodo);
                finish(); // dismissed the activity, pops it from the stack
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}