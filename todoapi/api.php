<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


DB::$dbName = 'todoauthdb';
DB::$user = 'todoauthdb';
DB::$password = "yy0uXiOhMsFOyPDj";
DB::$host = '127.0.0.1'; // 127.0.0.1 if it doesn't work
DB::$port =3306;

DB::$encoding = 'utf8';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

function error_handler($params) {
    global $log;
    if (isset($params['query'])) {
        $log->error("SQL Error: " . $params['error']);
        $log->error("SQL Query: " . $params['query']);
    } else {
        $log->error("Database Error: " . $params['error']);
    }
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error');
    die; // don't want to keep going if a query broke
}


// returns FALSE if authentication is missing or invalid
// returns users table record of authenticated users if user/pass was correct
function getAuthUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        return FALSE;
    }
    $email = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];
    $authUser = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if (!$authUser) {
        return FALSE;
    }
    if ($authUser['password'] == $password) {
        unset($authUser['password']);
        return $authUser;
    } else {
        return FALSE;
    }
}

// returns TRUE if data is valid, otherwise string describing the problem
function isUserValid($user) {
    if (is_null($user)) return "JSON parsing failed, user is null";
    if (count($user) != 2) return "Invalid number of values received";
    if (filter_var($user['email'], FILTER_VALIDATE_EMAIL) === FALSE) return "Email is invalid";
    // TODO: require quality passwords, e.g. one upper-case, one lower-case, one digit or special character
    if (strlen($user['password']) < 8) return "Password too short, must be 8 characters minimum";
    return TRUE;
}

// returns TRUE if data is valid, otherwise string describing the problem
function isTodoValid($todo) {
    if (is_null($todo)) return "JSON parsing failed, todo is null";
    if (count($todo) != 3) return "Invalid number of values received";
    if (strlen($todo['task']) < 1 || strlen($todo['task']) > 100)
        return "Task too short or too long, must be 1-100 characters";
    if (date_create_from_format("Y-m-j", $todo['dueDate']) === FALSE)  return "Due date is invalid";
    if (!in_array($todo['isDone'], array('pending', 'done'))) return "Is done invalid, must be pending or done";
    return TRUE;
}


$app = new \Slim\Slim();

// this script will always return JSON to any client
$app->response()->header('content-type', 'application/json');

// Modify PHP Slim main error handler to produce JSON and write to log
// TO BE TESTED !!!
$app->error(function (\Exception $e) use ($app, $log) {
    $log->error($e);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error (exception)');
});

$app->notFound(function() use ($app){
    $app->response()->status(404);
    echo json_encode("404 - not found");
});

// Retreive user by their email
$app->get('/users/:email', function($email) use ($app) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    // only allow to view one's own record
    if ($user['id'] == $authUser['id']) {
        unset($user['password']); // do NOT send password back
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

// Create a new user, by entering email and password in body
$app->post('/users', function() use ($app) { // NO AUTHENTICATION REQUIRED
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify user data is valid
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // make sure email is not already in use
    $isEmailInUse = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE email=%s", $data['email']);
    if ($isEmailInUse) {
        echo json_encode("400 - email already in use");
        $app->response()->status(400);
        return;
    }
    DB::insert('users', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});


// TODO: Log user's creation, changing password or email

// Update user info, find user by email, can change email or password
$app->put('/users/:email', function($email) use ($log, $app) {
    // FIXME: verify todo data is valid
    // FIXME: fail with 400 if record does not exist
    // FIXME: fail if email is already used with another account
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify todo data is valid   
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: fail with 400 if record does not exist
    $hasOldUser = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE id=%i AND email=%s", $authUser['id'], $email);
    if (!$hasOldUser) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('users', $data, 'email=%s', $email);
    echo json_encode(true);
});

// Retrieve all users, one of the already existed user
$app->get('/todos', function() use ($app, $log) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $todosList = DB::query("SELECT * FROM todos WHERE userId=%i", $authUser['id']);
    echo json_encode($todosList, JSON_PRETTY_PRINT);
});


// Create a new todo, by entering task, dueDate and isDone, one of the already existed user
$app->post('/todos', function() use ($app, $log) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    $result = isTodoValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: verify todo data is valid
    $data['userId'] = $authUser['id'];
    DB::insert('todos', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    $log->debug("todo created with id=" . $id);
    echo json_encode($id);
});

// Retrieve one todo from its id, one of the already existed user
$app->get('/todos/:id', function($id) use ($app) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i AND userId=%i", $id, $authUser['id']);
    if ($todo) {
        echo json_encode($todo, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

// Update a todos, find todo by its id, can change task, dueDate and isDone
$app->put('/todos/:id', function($id) use ($app) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify todo data is valid   
    $result = isTodoValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: fail with 400 if record does not exist
    $hasOldTodo = DB::queryFirstField("SELECT COUNT(*) FROM todos WHERE id=%i AND userId=%i", $id, $authUser['id']);
    if (!$hasOldTodo) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('todos', $data, 'id=%d AND userId=%i', $id, $authUser['id']);
    echo json_encode(true);
});

// Delete a todo, find todo by id, delete it
$app->delete('/todos/:id', function($id) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    // MAYBE-TODO: if record exists and it's not authUser's then return 403 instead
    DB::delete('todos', 'id=%i AND userId=%i', $id, $authUser['id']);
    echo json_encode(DB::affectedRows() != 0);
});

$app->run();