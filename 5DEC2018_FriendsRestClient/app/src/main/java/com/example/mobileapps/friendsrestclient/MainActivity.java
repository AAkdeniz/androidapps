package com.example.mobileapps.friendsrestclient;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_API_URL = "http://10.0.2.2:8801/api.php";      //"https://jacmap.000webhostapp.com/api.php";   //"http://10.0.2.2:8801/api.php";

    public static final String TAG = "MainActivity";
    public static final String EXTRA_INDEX = "index";

    ArrayList<Friend> friendList = new ArrayList<>();
    ArrayAdapter<Friend> friendAdapter;

    ListView lvFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        lvFriends = (ListView) findViewById(R.id.lvFriends);
        friendAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friendList);
        lvFriends.setAdapter(friendAdapter);


        //******************* On Long Click ******************//
        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Friend clickedfriend =(Friend) parent.getItemAtPosition(position);
                showDeleteFriendDialogue(clickedfriend);
                return true;
            }

        });
        //------------------------------------------------------------------//


        //******************* On Click ******************//
        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int postionclicked = position;
                String nameClicked = (String) parent.getItemAtPosition(position).toString();               //
                int myid = Integer.parseInt(nameClicked.substring(0,1));
                Friend myfriend =  (Friend)parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                String fid = myfriend.id+"";
                String fage = myfriend.age+"";
                intent.putExtra("id",fid);
                intent.putExtra("name",myfriend.name);
                intent.putExtra("age",fage);
                startActivity(intent);

            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadFriendsListAsyncTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.mi_sort_by_id: {
                new LoadFriendsListAsyncTask().execute();
                return true;
            }
            case R.id.mi_sort_by_name: {
                new SortByNameFriendsListAsyncTask().execute();
                return true;
            }
            case R.id.mi_sort_by_age: {
                new SortByAgeFriendsListAsyncTask().execute();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class LoadFriendsListAsyncTask extends AsyncTask<Void, Void, ArrayList<Friend>> {

        public static final String TAG = "LoadFriendsListAsyncTas";

        @Override
        protected ArrayList<Friend> doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /friends");
                ArrayList<Friend> result = new ArrayList<>();
                URL url = new URL(MainActivity.BASE_API_URL + "/friends");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();
                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();
                    // name
                    key = reader.nextName();
                    if (!key.equals("name")) throw new ParseException("name expected when parsing", 0);
                    String name = reader.nextString();
                    // age
                    key = reader.nextName();
                    if (!key.equals("age")) throw new ParseException("age expected when parsing", 0);
                    int age = reader.nextInt();
                    //
                    reader.endObject();
                    Friend friend = new Friend(id, name, age);
                    result.add(friend);
                    Log.v(TAG, "Friend parsed and added to list: " + friendList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return null;
            }
            // FIXME: finally missing closing
        }

        @Override
        protected  void onPostExecute(ArrayList<Friend> result) {
            friendList.clear();
            if (result == null) {
                Toast.makeText(MainActivity.this, "API error fetching data", Toast.LENGTH_LONG).show();
                return;
            }
            for (Friend friend : result) {
                friendList.add(friend);
            }
            friendAdapter.notifyDataSetChanged();
        }
    }


    class SortByNameFriendsListAsyncTask extends AsyncTask<Void, Void, ArrayList<Friend>> {

        public static final String TAG = "SortByNameAsyncTask";

        @Override
        protected ArrayList<Friend> doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /friends");
                ArrayList<Friend> result = new ArrayList<>();
                URL url = new URL(MainActivity.BASE_API_URL + "/friendsSortByName");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();
                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();
                    // name
                    key = reader.nextName();
                    if (!key.equals("name")) throw new ParseException("name expected when parsing", 0);
                    String name = reader.nextString();
                    // age
                    key = reader.nextName();
                    if (!key.equals("age")) throw new ParseException("age expected when parsing", 0);
                    int age = reader.nextInt();
                    //
                    reader.endObject();
                    Friend friend = new Friend(id, name, age);
                    result.add(friend);
                    Log.v(TAG, "Friend parsed and added to list: " + friendList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return null;
            }
            // FIXME: finally missing closing
        }

        @Override
        protected  void onPostExecute(ArrayList<Friend> result) {
            friendList.clear();
            if (result == null) {
                Toast.makeText(MainActivity.this, "API error sorting by name", Toast.LENGTH_LONG).show();
                return;
            }
            for (Friend friend : result) {
                friendList.add(friend);
            }
            friendAdapter.notifyDataSetChanged();
        }
    }

    class SortByAgeFriendsListAsyncTask extends AsyncTask<Void, Void, ArrayList<Friend>> {

        public static final String TAG = "SortByNameAsyncTask";

        @Override
        protected ArrayList<Friend> doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /friends");
                ArrayList<Friend> result = new ArrayList<>();
                URL url = new URL(MainActivity.BASE_API_URL + "/friendsSortByAge");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();
                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();
                    // name
                    key = reader.nextName();
                    if (!key.equals("name")) throw new ParseException("name expected when parsing", 0);
                    String name = reader.nextString();
                    // age
                    key = reader.nextName();
                    if (!key.equals("age")) throw new ParseException("age expected when parsing", 0);
                    int age = reader.nextInt();
                    //
                    reader.endObject();
                    Friend friend = new Friend(id, name, age);
                    result.add(friend);
                    Log.v(TAG, "Friend parsed and added to list: " + friendList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return null;
            }
            // FIXME: finally missing closing
        }

        @Override
        protected  void onPostExecute(ArrayList<Friend> result) {
            friendList.clear();
            if (result == null) {
                Toast.makeText(MainActivity.this, "API error sorting by name", Toast.LENGTH_LONG).show();
                return;
            }
            for (Friend friend : result) {
                friendList.add(friend);
            }
            friendAdapter.notifyDataSetChanged();
        }
    }

    //******************************** Delete *********************************//
    class DeleteFriendAsyncTask extends AsyncTask<Friend, Void, Boolean> {

        public static final String TAG = "DeleteFriendAsyncTask";

        @Override
        protected Boolean doInBackground(Friend... params) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /friends");
                ArrayList<Friend> result = new ArrayList<>();
                Friend selectedfriend = params[0];
                int id = selectedfriend.id;
                URL url = new URL(MainActivity.BASE_API_URL + "/friends/"+id);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("DELETE");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);

                return true;
            } catch (IOException  ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return false;
            }
            // FIXME: finally missing closing
        }

        @Override
        protected  void onPostExecute(Boolean result) {
            friendList.clear();
            if (result == false) {
                Toast.makeText(MainActivity.this, "API error deleting friend", Toast.LENGTH_LONG).show();
                return ;
            }else{
                new LoadFriendsListAsyncTask().execute();
                Toast.makeText(MainActivity.this, "Friend Deleted Successfully", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void showDeleteFriendDialogue(final Friend friend) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Do you want to delete \n'" + friend.getName()+ "'");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteFriendAsyncTask().execute (friend);
               // friendAdapter.notifyDataSetChanged();
               // Toast.makeText(MainActivity.this, friend.getName() + " is deleted", Toast.LENGTH_SHORT).show();
                Log.d("Main Activity", "Friend '" + friend.getName() + "' deleted.");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }


}
