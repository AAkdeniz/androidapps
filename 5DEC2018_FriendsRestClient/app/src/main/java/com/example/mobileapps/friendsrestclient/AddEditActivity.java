package com.example.mobileapps.friendsrestclient;

import android.content.Intent;
import android.icu.util.EthiopicCalendar;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

public class AddEditActivity extends AppCompatActivity {



    EditText etName, etAge;
    TextView tvId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);

        tvId = (TextView) findViewById(R.id.tvId);
        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {

        } else {

            tvId.setText(extras.getString("id"));
            etName.setText(extras.getString("name"));
            etAge.setText(extras.getString("age"));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        Bundle extras = getIntent().getExtras();
        if (extras == null ){
       menuInflater.inflate(R.menu.add_menu ,menu);
        }else {
            menuInflater.inflate(R.menu.edit_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                // FIXME: verify inputs make sense
                String name = etName.getText().toString();
                int age = Integer.parseInt(etAge.getText().toString()); // FIXME: exception
                Friend friend = new Friend(0, name, age);
                new AddFriendAsyncTask().execute(friend);
                return true;
            }
            case R.id.mi_save: {
                // FIXME: verify inputs make sense
                String name = etName.getText().toString();
                int age = Integer.parseInt(etAge.getText().toString()); // FIXME: exception
                int id = Integer.parseInt(tvId.getText().toString());
                Friend friend = new Friend(id, name, age);
                new UpdateFriendAsyncTask().execute(friend);
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class AddFriendAsyncTask extends AsyncTask<Friend, Void, Boolean> {

        public static final String TAG = "AddFriendAsyncTask";

        @Override
        protected Boolean doInBackground(Friend... paramsList) {
            Friend friend = paramsList[0];
            HttpURLConnection connection = null;
            try {
                Log.v(TAG, "POST /friends " + friend.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/friends");
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                // String json = String.format("{ \"name\": \"%s\", \"age\": \"%d\" }", friend.name, friend.age);
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("name", friend.name);
                jsonData.put("age", friend.age);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close(); // FIXME - should be done in finally
                //
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }
            // FIXME: finally missing closing print writer, connection
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(AddEditActivity.this, "Friend added", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(AddEditActivity.this, "API error adding Friend", Toast.LENGTH_LONG).show();
            }
        }
    }

    //******************************** Update *********************************//
    class UpdateFriendAsyncTask extends AsyncTask<Friend, Void, Boolean> {

        public static final String TAG = "UpdateFriendAsyncTask";

        @Override
        protected Boolean doInBackground(Friend... paramsList) {
            Friend friend = paramsList[0];
            HttpURLConnection connection = null;
            int id = friend.id;
            try {
                Log.v(TAG, "Update /friends " + friend.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/friends/" + id);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                // String json = String.format("{ \"name\": \"%s\", \"age\": \"%d\" }", friend.name, friend.age);
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("id", friend.id);
                jsonData.put("name", friend.name);
                jsonData.put("age", friend.age);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close(); // FIXME - should be done in finally
                //
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }
            // FIXME: finally missing closing print writer, connection
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (result == false) {
                Toast.makeText(AddEditActivity.this, "API error deleting friend", Toast.LENGTH_LONG).show();
                return;
            } else {

                Toast.makeText(AddEditActivity.this, "Friend Deleted Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

        }
    }


}
