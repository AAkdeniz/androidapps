package com.mobileapps.aca.quiz4auctions;

import java.math.BigDecimal;

public class Item {

    int id;
    int ownerId;
    String ownerEmail;
    String description;
    BigDecimal highestBid;
    String highestBidderEmail;

    public Item(int id, int ownerId, String ownerEmail, String description, BigDecimal highestBid, String highestBidderEmail) {
        this.id = id;
        this.ownerId = ownerId;
        this.ownerEmail = ownerEmail;
        this.description = description;
        this.highestBid = highestBid;
        this.highestBidderEmail = highestBidderEmail;
    }

    public String toString() {
        return String.format("%s bid %s by %s", description, highestBid.toString(), highestBidderEmail);
    }
}
