package com.mobileapps.aca.quiz4auctions;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;

public class PlaceBidActivity extends AppCompatActivity {

    EditText etHighestBidderEmail, etHighestBid;
    TextView tvDescription, tvId, tvOwnerId , tvOwnerEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_bid);

        tvId = (TextView) findViewById(R.id.tvId);
        tvOwnerId = (TextView) findViewById(R.id.tvOwnerId);
        tvOwnerEmail = (TextView) findViewById(R.id.tvOwnerEmail);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        etHighestBid = (EditText) findViewById(R.id.etHighestBidderEmail);
        etHighestBidderEmail = (EditText) findViewById(R.id.etHighestBidderEmail);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {

        } else {

            tvId.setText(extras.getString("id"));
            tvOwnerId.setText(extras.getString("ownerId"));
            tvOwnerEmail.setText(extras.getString("ownerEmail"));
            tvDescription.setText(extras.getString("description"));
            etHighestBid.setText(extras.getString("highestBid"));
            etHighestBidderEmail.setText(extras.getString("highestBidderEmail"));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.place_bid_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.mi_place_bid: {

                int id = Integer.parseInt(tvId.getText().toString());

                int ownerId=1;
                try{
                ownerId = Integer.parseInt(tvOwnerId.getText().toString());
                }catch (NumberFormatException ex){
                    Toast.makeText(this, "NumberFormatException in owner id", Toast.LENGTH_SHORT).show();
                }

                String ownerEmail = tvOwnerEmail.getText().toString();

                String description = tvDescription.getText().toString();

                BigDecimal highestBid= new BigDecimal(100);

                try {

                    highestBid = new BigDecimal(etHighestBid.getText().toString()); // FIXME: exception
                }catch (NumberFormatException ex){
                    Toast.makeText(this, "NumberFormatException in highest bid", Toast.LENGTH_SHORT).show();
                }

                String highestBidderEmail = etHighestBidderEmail.getText().toString();




                Item currentItem = new Item(id, ownerId, ownerEmail, description, highestBid, highestBidderEmail);
                new UpdateItemAsyncTask().execute(currentItem);
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //******************************** Update *********************************//
    class UpdateItemAsyncTask extends AsyncTask<Item, Void, Boolean> {

        public static final String TAG = "UpdateItemAsyncTask";

        @Override
        protected Boolean doInBackground(Item... paramsList) {
            Item item = paramsList[0];
            HttpURLConnection connection = null;
            int id = item.id;
            try {
                Log.v(TAG, "Update /items " + item.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/items/" + id);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                // String json = String.format("{ \"name\": \"%s\", \"age\": \"%d\" }", item.name, item.age);
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("ownerId", item.ownerId);
                jsonData.put("description", item.description);
                jsonData.put("highestBidderEmail", item.highestBidderEmail);
                jsonData.put("highestBid", item.highestBid);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close();

                // FIXME - should be done in finally

                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }
            // FIXME: finally missing closing print writer, connection
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (result == false) {
                Toast.makeText(PlaceBidActivity.this, "API error deleting item", Toast.LENGTH_LONG).show();
                return;
            } else {

                Toast.makeText(PlaceBidActivity.this, "Item Deleted Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

        }
    }

}

