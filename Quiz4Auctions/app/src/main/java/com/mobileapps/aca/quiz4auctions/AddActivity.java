package com.mobileapps.aca.quiz4auctions;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddActivity extends AppCompatActivity {


    EditText etHighestBid, etDescription,etOwnerEmail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        etDescription = (EditText) findViewById(R.id.etDescription);
        etOwnerEmail = (EditText) findViewById(R.id.etOwnerEmail);
        etHighestBid = (EditText) findViewById(R.id.etHighestBidderEmail);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {

        } else {

            etDescription.setText(extras.getString("description"));
            etOwnerEmail.setText(extras.getString("ownerEmail"));
            etHighestBid.setText(extras.getString("highestBid"));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {

                String description = etDescription.getText().toString();
                BigDecimal highestBid = new BigDecimal(etHighestBid.getText().toString()); // FIXME: exception
                String ownerEmail = etOwnerEmail.getText().toString();


                Item newItem = new Item(0, 0,ownerEmail, description, highestBid, "");
                new AddItemAsyncTask().execute(newItem);
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class AddItemAsyncTask extends AsyncTask<Item, Void, Boolean> {

        public static final String TAG = "AddItemAsyncTask";

        @Override
        protected Boolean doInBackground(Item... paramsList) {
            Item item = paramsList[0];
            HttpURLConnection connection = null;
            try {
                Log.v(TAG, "POST /items " + item.toString());
                URL url = new URL(MainActivity.BASE_API_URL + "/items");
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(false); // not receiving data
                connection.setDoOutput(true); // but sending data
                //
                PrintWriter printWriter = new PrintWriter(connection.getOutputStream());
                // String json = String.format("{ \"name\": \"%s\", \"age\": \"%d\" }", item.name, item.age);
                JSONObject jsonData = new JSONObject();  // "{ }"
                jsonData.put("description", item.description);
                jsonData.put("highestBid", item.highestBid);
                jsonData.put("ownerEmail", item.ownerEmail);
                String jsonString = jsonData.toString();
                printWriter.print(jsonString);
                printWriter.flush();
                printWriter.close();

                // FIXME - should be done in finally

                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) {
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                return true;
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Exception writing to URL", ex);
                return false;
            }
            // FIXME: finally missing closing print writer, connection
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(AddActivity.this, "Item added", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(AddActivity.this, "API error adding Item", Toast.LENGTH_LONG).show();
            }
        }
    }

}
