package com.mobileapps.aca.quiz4auctions;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_API_URL = "http://10.0.2.2:8804/api.php";

    public static final String TAG = "MainActivity";
    public static final String EXTRA_INDEX = "index";

    ArrayList<Item> itemList = new ArrayList<>();
    ArrayAdapter<Item> itemAdapter;

    ListView lvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        lvItems = (ListView) findViewById(R.id.lvItems);
        itemAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, itemList);
        lvItems.setAdapter(itemAdapter);


        //******************* On Click ******************//
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = (Item) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, PlaceBidActivity.class);

                String itemId = item.id + "";
                String itemDescription = item.description + "";
                String itemCurrentOwnerEmail = item.ownerEmail + "";


                intent.putExtra("id", itemId);
                intent.putExtra("description", itemDescription);
                intent.putExtra("ownerEmail", itemCurrentOwnerEmail);

                startActivity(intent);

            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadItemsListAsyncTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_new_auction: {
                Intent intent = new Intent(this, AddActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.mi_account: {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class LoadItemsListAsyncTask extends AsyncTask<Void, Void, ArrayList<Item>> {

        public static final String TAG = "LoadItemsListAsyncTas";

        @Override
        protected ArrayList<Item> doInBackground(Void... voids) {
            InputStreamReader input = null;
            JsonReader reader = null;
            try {
                Log.v(TAG, "GET /items");
                ArrayList<Item> result = new ArrayList<>();
                URL url = new URL(MainActivity.BASE_API_URL + "/items");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                input = new InputStreamReader(connection.getInputStream());
                int httpCode = connection.getResponseCode();
                if (httpCode / 100 != 2) { // not 2xx means problems
                    throw new IOException("Invalid HTTP code response: " + httpCode);
                }
                reader = new JsonReader(input);
                reader.beginArray();
                while (reader.hasNext()) {
                    String key;
                    reader.beginObject();

                    // id
                    key = reader.nextName();
                    if (!key.equals("id")) throw new ParseException("id expected when parsing", 0);
                    int id = reader.nextInt();

                    // ownerId
                    key = reader.nextName();
                    if (!key.equals("ownerId")) throw new ParseException("ownerId expected when parsing", 0);
                    int ownerId = reader.nextInt();

                    // ownerEmail
                    key = reader.nextName();
                    if (!key.equals("ownerEmail"))
                        throw new ParseException("ownerEmail expected when parsing", 0);
                    String ownerEmail = reader.nextString();

                    // description
                    key = reader.nextName();
                    if (!key.equals("description"))
                        throw new ParseException("description expected when parsing", 0);
                    String description = reader.nextString();

                    // highestBid
                    key = reader.nextName();
                    if (!key.equals("highestBid")) throw new ParseException("highestBid expected when parsing", 0);
                    String highestBidStr = reader.nextString();
                    BigDecimal highestBid = new BigDecimal(highestBidStr);

                    // highestBidderEmail
                    key = reader.nextName();
                    if (!key.equals("highestBidderEmail"))
                        throw new ParseException("highestBidderEmail expected when parsing", 0);
                    String highestBidderEmail = reader.nextString();

                    //
                    reader.endObject();
                    Item item = new Item(id,ownerId,ownerEmail, description, highestBid, highestBidderEmail);
                    result.add(item);
                    Log.v(TAG, "Item parsed and added to list: " + itemList.toString());
                }
                reader.endArray();
                return result;
            } catch (IOException | ParseException ex) {
                Log.e(TAG, "Exception reading from URL", ex);
                return null;
            }
            // FIXME: finally missing closing
        }

        @Override
        protected void onPostExecute(ArrayList<Item> result) {
            itemList.clear();
            if (result == null) {
                Toast.makeText(MainActivity.this, "API error fetching data", Toast.LENGTH_LONG).show();
                return;
            }
            for (Item item : result) {
                itemList.add(item);
            }
            itemAdapter.notifyDataSetChanged();
        }
    }
}
