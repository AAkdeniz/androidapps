package com.mobileapps.aca.a27nov2018_friendswithphotos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertFriend(Friend friend);

    @Query("SELECT * FROM Friends WHERE id = :friendId")
    Friend fetchFriendById(int friendId);

    @Query("SELECT * FROM Friends")
    List<Friend> fetchAllFriends();

    @Update
    void updateFriend(Friend friend);

    @Delete
    void deleteFriend(Friend friend);
}