package com.mobileapps.aca.a27nov2018_friendswithphotos;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String DATABASE_NAME = "friends_db";
    public static final String EXTRA_INDEX = "index";
    private static final String TAG = "MainActivity";

    private AppDatabase appDatabase;

    public static ArrayList<Friend> friendsList= new ArrayList<>();
    public static ArrayAdapter<Friend> friendAdapter;

    ListView lvFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
        //
        lvFriends = (ListView) findViewById(R.id.lvFriends);

        friendAdapter = new FriendArrayAdapter(this,friendsList);
        lvFriends.setAdapter(friendAdapter);


        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // ItemView was clicked, find out which String (name) it was displaying
            Friend friendClicked = (Friend) parent.getItemAtPosition(position);
            int index = friendsList.indexOf(friendClicked);
            Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
            intent.putExtra(EXTRA_INDEX, index);
            startActivity(intent);
        }
    });

        onLongClickForDelete();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onLongClickForDelete() {
        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, int i, long l) {

                Friend friendClicked = (Friend) adapterView.getItemAtPosition(i);
                showDeleteFriendDialog(friendClicked);
                return true;
            }
        });
    }

    private void deleteFriendDbAsyncTask(Friend friend) {
        new DeleteFriendDbAsyncTask().execute(friend);
        friendAdapter.notifyDataSetChanged();
        Toast.makeText(MainActivity.this, friend.getFirstName()+" "+friend.getLastName() + " is deleted!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Friend deleted: " + friend.getFirstName()+" "+friend.getLastName());
    }



    private void showDeleteFriendDialog(final Friend friend) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Friend");
        builder.setMessage("Are you sure you want to delete \n" + friend.getFirstName()+" "+friend.getLastName());

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                try {
                    deleteFriendDbAsyncTask(friend);
                } catch (Exception ex) {
                    Log.e(TAG, "DaoAccess.deleteFriend() failed", ex);

                }

                Toast.makeText(MainActivity.this, friend.getFirstName()+" "+friend.getLastName() + " is deleted", Toast.LENGTH_LONG).show();
                Log.d("Main Activity", "Friend deleted: " + friend.getFirstName()+" "+friend.getLastName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    class DeleteFriendDbAsyncTask extends AsyncTask<Friend, Void, Void> {

        @Override
        protected Void doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().deleteFriend(friend);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.deleteFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadFriendsFromDbAsyncTask().execute(); // reload the list
        }
    }


    class LoadFriendsFromDbAsyncTask extends AsyncTask<Void, Void, List<Friend>> {

        @Override
        protected List<Friend> doInBackground(Void... params) {
            try {
                List<Friend> list = appDatabase.daoAccess().fetchAllFriends();
                return list;

            } catch (Exception ex) {
                Log.e(TAG, "Database error when loading friend list", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Friend> list) {
            if (list == null) {
                Toast.makeText(MainActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                friendsList.clear();
                for (Friend friend : list) {
                    friendsList.add(friend);
                }
                friendAdapter.notifyDataSetChanged();
            }
        }
    }

    public class FriendArrayAdapter extends ArrayAdapter<Friend> {

        private Context context;
        private ArrayList<Friend> list;

        public FriendArrayAdapter(Context context, ArrayList<Friend> list) {
            super(context, R.layout.friend_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // FIXME: reuse convert view if available
            View rowView = inflater.inflate(R.layout.friend_item, parent, false);
            TextView tvFirstLastName = (TextView) rowView.findViewById(R.id.tvFirstLastName);
            TextView tvDateOfBirth = (TextView) rowView.findViewById(R.id.tvDateOfBirth);
            ImageView ivPhoto = (ImageView) rowView.findViewById(R.id.ivPhoto);
            //
            Friend friend = list.get(position);
            tvFirstLastName.setText(friend.firstName+" "+friend.lastName);
            tvDateOfBirth.setText(friend.dateOfBirth); // FIXME: should format date

            try {
                Uri uri = Uri.parse(friend.photoUri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                ivPhoto.setImageBitmap(bitmap);

            } catch (IOException ex) {
                Log.e(TAG, "Exception loading bitmap failed " + friend.photoUri, ex);
            }

            return rowView;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new LoadFriendsFromDbAsyncTask().execute();
    }
}
