package com.mobileapps.aca.a27nov2018_friendswithphotos;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;

import java.net.URI;
import java.util.Date;
import java.util.Objects;


@Entity(tableName = "Friends")
public class Friend {

    @PrimaryKey(autoGenerate = true)
    private int id;

    String firstName;
    String lastName;
    String dateOfBirth;
    String photoUri;

    public Friend(int id, String firstName, String lastName, String dateOfBirth, String photoUri) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.photoUri = photoUri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhotoURI() {
        return photoUri;
    }

    public void setPhotoURI(String photoURI) {
        this.photoUri = photoURI;
    }

    @Override
    public String toString() {
        return String.format("%d: %s: %s: %s: %s", id, firstName, lastName, dateOfBirth, photoUri);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friend friend = (Friend) o;
        return id == friend.id &&
                Objects.equals(firstName, friend.firstName) &&
                Objects.equals(lastName, friend.lastName) &&
                Objects.equals(dateOfBirth, friend.dateOfBirth) &&
                Objects.equals(photoUri, friend.photoUri);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, dateOfBirth, photoUri);
    }
}
