package com.mobileapps.aca.a27nov2018_friendswithphotos;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class AddEditActivity extends AppCompatActivity {

    ImageView ivPhoto;
    EditText etFirstName;
    EditText etLastName;
    EditText etDateOfBirth;
    Uri uri = null;

    Friend currentFriend;
    private static final String DATABASE_NAME = "friends_db";
    private static final String IMAGE_DIRECTORY = "/sdcard/Download";
    private static final String TAG = "AddEditActivity";
    private int GALLERY = 1;

    AppDatabase appDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();

        init();

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        Intent intent = getIntent();
        int index = intent.getIntExtra(MainActivity.EXTRA_INDEX, -1);
        currentFriend = (index == -1) ? null : MainActivity.friendsList.get(index);
        if (currentFriend != null) {
            etFirstName.setText(currentFriend.firstName);
            etLastName.setText(currentFriend.lastName);
            etDateOfBirth.setText(currentFriend.dateOfBirth);
            ivPhoto.setImageResource(R.drawable.ic_launcher_foreground);


            // FIXME: setting photo does not work
        }


    }

    public void init() {
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etDateOfBirth = (EditText) findViewById(R.id.etDateOfBirth);
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        choosePhotoFromGallery();
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    uri = contentURI;
                    String path = saveImage(bitmap);
                    Toast.makeText(AddEditActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    ivPhoto.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AddEditActivity.this, "Image saving failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        // TODO: If editing, load edit_menu instead
        menuInflater.inflate(currentFriend == null ? R.menu.add_menu : R.menu.edit_menu, menu);
        return true;
    }

    String photoUri;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();

                String dateOfBirth = etDateOfBirth.getText().toString();

                photoUri = uri.toString();

                Friend friend = new Friend(0, firstName, lastName, dateOfBirth, photoUri);
                new InsertFriendDbAsyncTask().execute(new Friend(0, firstName, lastName, dateOfBirth, photoUri));
                MainActivity.friendsList.add(friend);

                // FIXME: MainActivity needs to refresh list in onStart();
                MainActivity.friendAdapter.notifyDataSetChanged();
                finish();
                return true;
            }
            case R.id.mi_edit: {
                currentFriend.firstName = etFirstName.getText().toString();
                currentFriend.lastName = etLastName.getText().toString();
                currentFriend.dateOfBirth = etDateOfBirth.getText().toString();
                currentFriend.photoUri = photoUri;

                updateFriendDbAsyncTask(currentFriend);
                finish();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class InsertFriendDbAsyncTask extends AsyncTask<Friend, Void, Void> {

        @Override
        protected Void doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().insertFriend(friend);
                //return true;
            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.insertFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (result != null) {
                new LoadFriendsFromDbAsyncTask().execute(); // reload the list
                Toast.makeText(AddEditActivity.this, "Friend added", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddEditActivity.this, "Database error when adding Friend", Toast.LENGTH_LONG).show();
            }
        }
    }

    class LoadFriendsFromDbAsyncTask extends AsyncTask<Void, Void, List<Friend>> {

        @Override
        protected List<Friend> doInBackground(Void... params) {

            try {
                List<Friend> list = appDatabase.daoAccess().fetchAllFriends();
                return list;

            } catch (Exception ex) {
                Log.e(TAG, "Database error when loading friend list", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Friend> list) {
            if (list == null) {
                Toast.makeText(AddEditActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                MainActivity.friendsList.clear();
                for (Friend friend : list) {
                    MainActivity.friendsList.add(friend);
                }
                MainActivity.friendAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new LoadFriendsFromDbAsyncTask().execute();
    }

    private void updateFriendDbAsyncTask(Friend friend) {
        new UpdateFriendDbAsyncTask().execute(friend);
        MainActivity.friendAdapter.notifyDataSetChanged();
        Toast.makeText(AddEditActivity.this, friend.getFirstName() + " " + friend.getLastName() + " is updated!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Friend updated: " + friend.getFirstName() + " " + friend.getLastName());
    }


    class UpdateFriendDbAsyncTask extends AsyncTask<Friend, Void, Void> {

        @Override
        protected Void doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().updateFriend(friend);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.updateFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadFriendsFromDbAsyncTask().execute(); // reload the list
        }

    }

}
