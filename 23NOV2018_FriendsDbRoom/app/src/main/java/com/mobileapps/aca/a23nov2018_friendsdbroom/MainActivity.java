package com.mobileapps.aca.a23nov2018_friendsdbroom;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String DATABASE_NAME = "friends_db";
    private static final String TAG = "MainActivity";

    private AppDatabase appDatabase;

    ListView lvFriends;
    List<Friend> friendsList;
    ArrayAdapter<Friend> friendsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
        //
        lvFriends = (ListView) findViewById(R.id.lvFriends);

        friendsList = new ArrayList<>();

        friendsAdapter = new ArrayAdapter<Friend>(this, android.R.layout.simple_list_item_1, friendsList);
        lvFriends.setAdapter(friendsAdapter);

        onLongClickForDelete();

        onClickForUpdate();
    }

    public void onLongClickForDelete() {
        lvFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, int i, long l) {

                Friend friendClicked = (Friend) adapterView.getItemAtPosition(i);
                showDeleteFriendDialog(friendClicked);
                return true;
            }
        });
    }

    public void onClickForUpdate() {
        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Friend friendEdited = (Friend) adapterView.getItemAtPosition(i);
                showAddEditFriendDialog(friendEdited);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadFriendsFromDbAsyncTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add:
                showAddEditFriendDialog(null);
                return true;
            case R.id.mi_sort:
               // sortFriendsByName(List<Friend> friendsList);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void showAddEditFriendDialog(final Friend friendEdited) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(friendEdited == null ? "Add friend" : "Edit friend");
        // setup the additional edit text
        final EditText etFriendName = new EditText(this);
        etFriendName.setInputType(InputType.TYPE_CLASS_TEXT);
        etFriendName.setText(friendEdited == null ? "" : friendEdited.getName());
        builder.setView(etFriendName);
        builder.setPositiveButton(friendEdited == null ? "Add" : "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newName = etFriendName.getText().toString();

                if (friendEdited == null) { // add
                    new InsertFriendDbAsyncTask().execute(new Friend(0, newName));
                } else { // save
                    friendEdited.setName(newName);
                    updateFriendDbAsyncTask(friendEdited);
                }

                friendsAdapter.notifyDataSetChanged();
                String action = friendEdited == null ? "added" : "updated";
                Toast.makeText(MainActivity.this, "Friend " + action, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Friend " + action + ": " + newName);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    class InsertFriendDbAsyncTask extends AsyncTask<Friend, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().insertFriend(friend);
                return true;
            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.insertFriend() failed", ex);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                new LoadFriendsFromDbAsyncTask().execute(); // reload the list
                Toast.makeText(MainActivity.this, "Friend added", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "Database error when adding Friend", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void deleteFriendDbAsyncTask(Friend friend) {
        new DeleteFriendDbAsyncTask().execute(friend);
        friendsAdapter.notifyDataSetChanged();
        Toast.makeText(MainActivity.this, friend.getName() + " is deleted!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Friend deleted: " + friend.getName());
    }

    private void showDeleteFriendDialog(final Friend friend) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Friend");
        builder.setMessage("Are you sure you want to delete \n" + friend.getName());

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                try {
                    deleteFriendDbAsyncTask(friend);
                } catch (Exception ex) {
                    Log.e(TAG, "DaoAccess.deleteFriend() failed", ex);

                }

                Toast.makeText(MainActivity.this, friend.getName() + " is deleted", Toast.LENGTH_LONG).show();
                Log.d("Main Activity", "Friend deleted: " + friend.getName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    class DeleteFriendDbAsyncTask extends AsyncTask<Friend, Void, Void> {

        @Override
        protected Void doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().deleteFriend(friend);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.deleteFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadFriendsFromDbAsyncTask().execute(); // reload the list
        }
    }

    class LoadFriendsFromDbAsyncTask extends AsyncTask<Void, Void, List<Friend>> {

        @Override
        protected List<Friend> doInBackground(Void... params) {
            try {
                List<Friend> list = appDatabase.daoAccess().fetchAllFriends();
                return list;

            } catch (Exception ex) {
                Log.e(TAG, "Database error when loading friend list", ex);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Friend> list) {
            if (list == null) {
                Toast.makeText(MainActivity.this, "Database error fetching data", Toast.LENGTH_LONG).show();
            } else {
                // WRONG: friendsList = list;
                friendsList.clear();
                for (Friend friend : list) {
                    friendsList.add(friend);
                }
                friendsAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateFriendDbAsyncTask(Friend friend) {
        new UpdateFriendDbAsyncTask().execute(friend);
        friendsAdapter.notifyDataSetChanged();
        Toast.makeText(MainActivity.this, friend.getName() + " is updated!", Toast.LENGTH_LONG).show();
        Log.d("Main Activity", "Friend updated: " + friend.getName());
    }


    class UpdateFriendDbAsyncTask extends AsyncTask<Friend, Void, Void> {

        @Override
        protected Void doInBackground(Friend... params) {
            Friend friend = params[0];
            try {
                appDatabase.daoAccess().updateFriend(friend);

            } catch (Exception ex) {
                Log.e(TAG, "DaoAccess.updateFriend() failed", ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new LoadFriendsFromDbAsyncTask().execute(); // reload the list
        }

    }
}

