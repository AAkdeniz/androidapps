package com.mobileapps.aca.a14nov2018_celsius_converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void convertToFahrenheit(View view) {
        TextView result = (TextView) findViewById(R.id.result);
        EditText editText = (EditText) findViewById(R.id.editText);

        String stringVariable = editText.getText().toString();
        Double doubleVariable = Double.parseDouble(stringVariable);
        doubleVariable = (doubleVariable * 1.8) + 32;

        String stringResult = doubleVariable + " Fahrenheit";

        result.setText(stringResult);
    }
}
