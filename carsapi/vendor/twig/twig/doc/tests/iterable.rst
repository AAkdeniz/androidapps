``iterable``
============

.. versionadded:: 1.7
    The iterable test was added in Twig 1.7.

``iterable`` checks if a variable is an array or a traversable object:

.. code-block:: jinja

    {# evaluates to true if the foo variable is iterable #}
    {% if owners is iterable %}
        {% for owner in owners %}
            Hello {{ owner }}!
        {% endfor %}
    {% else %}
        {# owners is probably a string #}
        Hello {{ owners }}!
    {% endif %}
