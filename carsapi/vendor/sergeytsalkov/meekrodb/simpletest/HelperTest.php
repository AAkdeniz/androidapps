<?php
class HelperTest extends SimpleTest {
  function test_1_verticalslice() {
    $all = DB::query("SELECT * FROM accounts ORDER BY id ASC");
    $names = DBHelper::verticalSlice($all, 'ownername');
    $this->assert(count($names) === 5);
    $this->assert($names[0] === 'Abe');
    
    $ages = DBHelper::verticalSlice($all, 'age', 'ownername');
    $this->assert(count($ages) === 5);
    $this->assert($ages['Abe'] === '700');
  }
  
  function test_2_reindex() {
    $all = DB::query("SELECT * FROM accounts ORDER BY id ASC");
    $names = DBHelper::reIndex($all, 'ownername');
    $this->assert(count($names) === 5);
    $this->assert($names['Bart']['ownername'] === 'Bart');
    $this->assert($names['Bart']['age'] === '15');
    
    $names = DBHelper::reIndex($all, 'ownername', 'age');
    $this->assert($names['Bart']['15']['ownername'] === 'Bart');
    $this->assert($names['Bart']['15']['age'] === '15');
  }
  
  function test_3_empty() {
    $none = DB::query("SELECT * FROM accounts WHERE ownername=%s", 'doesnotexist');
    $this->assert(is_array($none) && count($none) === 0);
    $names = DBHelper::verticalSlice($none, 'ownername', 'age');
    $this->assert(is_array($names) && count($names) === 0);
    
    $names_other = DBHelper::reIndex($none, 'ownername', 'age');
    $this->assert(is_array($names_other) && count($names_other) === 0);
  }
  
  function test_4_null() {
    DB::query("UPDATE accounts SET name = NULL WHERE ownername=%s", 'Bart');
    
    $all = DB::query("SELECT * FROM accounts ORDER BY id ASC");
    $ages = DBHelper::verticalSlice($all, 'age', 'name');
    $this->assert(count($ages) === 5);
    $this->assert($ages[''] === '15');
    
    $names = DBHelper::reIndex($all, 'name');
    $this->assert(count($names) === 5);
    $this->assert($names['']['ownername'] === 'Bart');
    $this->assert($names['']['name'] === NULL);
  }
  
}
?>
