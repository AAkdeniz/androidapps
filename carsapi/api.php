<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


DB::$dbName = 'carsdb';
DB::$owner = 'carsdb';
DB::$name = "carsdb";
DB::$host = '127.0.0.1'; // 127.0.0.1 if it doesn't work
DB::$port =3306;

DB::$encoding = 'utf8';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

function error_handler($params) {
    global $log;
    if (isset($params['query'])) {
        $log->error("SQL Error: " . $params['error']);
        $log->error("SQL Query: " . $params['query']);
    } else {
        $log->error("Database Error: " . $params['error']);
    }
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error');
    die; // don't want to keep going if a query broke
}


// returns FALSE if authentication is missing or invalid
// returns owners table record of authenticated owners if owner/pass was correct
function getAuthOwner() {
    if (!isset($_SERVER['PHP_AUTH_LICENCENUMBER'])) {
        return FALSE;
    }
    $licenceNumber = $_SERVER['PHP_AUTH_LICENCENUMBER'];
    $name = $_SERVER['PHP_AUTH_NAME'];
    $authOwner = DB::queryFirstRow("SELECT * FROM owners WHERE licenceNumber=%i", $licenceNumber);
    if (!$authOwner) {
        return FALSE;
    }
    if ($authOwner['name'] == $name) {
        unset($authOwner['name']);
        return $authOwner;
    } else {
        return FALSE;
    }
}

// returns TRUE if data is valid, otherwise string describing the problem
function isOwnerValid($owner) {
    if (is_null($owner)) return "JSON parsing failed, owner is null";
    if (count($owner) != 3) return "Invalid number of values received";
    //if (filter_var($owner['licenceNumber'], FILTER_VALIDATE_LICENCE) === FALSE) return "licenceNumber is invalid";   // Add the filter.php the  FILTER_VALIDATE_LICENCE constant
    // car: require quality names, e.g. one upper-case, one lower-case, one digit or special character
    if (date_create_from_format("Y-m-j", $owner['dateOfBirth']) === FALSE)  return "Birth date is invalid";
    if (strlen($owner['name']) < 2) return "Name too short, must be 2 characters minimum";
    return TRUE;
}

// returns TRUE if data is valid, otherwise string describing the problem
function isCarValid($car) {
    if (is_null($car)) return "JSON parsing failed, car is null";
    if (count($car) != 4) return "Invalid number of values received";
    if (strlen($car['makemodel']) < 5 || strlen($car['makemodel']) > 50)
        return "Car name too short or too long, must be 5-50 characters";
    if (!in_array($car['fuelType'], array('gasoline', 'diesel', 'electric'))) return "Fuel type is invalid, must be gasoline, diesel or electric";
    return TRUE;
}


$app = new \Slim\Slim();

// this script will always return JSON to any client
$app->response()->header('content-type', 'application/json');

// Modify PHP Slim main error handler to produce JSON and write to log
// TO BE TESTED !!!
$app->error(function (\Exception $e) use ($app, $log) {
    $log->error($e);
    http_response_code(500);
    header('content-type: application/json');
    echo json_encode('500 - internal error (exception)');
});

$app->notFound(function() use ($app){
    $app->response()->status(404);
    echo json_encode("404 - not found");
});

// Create a new owner, by entering licenceNumber,name and date of birth in body
$app->post('/owners', function() use ($app) { // NO AUTHENTICATION REQUIRED
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify owner data is valid
    $result = isOwnerValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // make sure licenceNumber is not already in use
    $islicenceNumberInUse = DB::queryFirstField("SELECT COUNT(*) FROM owners WHERE licenceNumber=%i", $data['licenceNumber']);
    if ($islicenceNumberInUse) {
        echo json_encode("400 - licenceNumber already in use");
        $app->response()->status(400);
        return;
    }
    DB::insert('owners', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    echo json_encode($id);
});

// Retreive owner by their licenceNumber
$app->get('/owners/:licenceNumber', function($licenceNumber) use ($app) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $owner = DB::queryFirstRow("SELECT * FROM owners WHERE licenceNumber=%i", $licenceNumber);
    // only allow to view one's own record
    if ($owner['id'] == $authOwner['id']) {
        unset($owner['name']); // do NOT send name back
        echo json_encode($owner, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});

// Update owner info, find owner by licenceNumber, can change name or licenceNumber
$app->put('/owners/:licenceNumber', function($licenceNumber) use ($log, $app) {
    // FIXME: verify car data is valid
    // FIXME: fail with 400 if record does not exist
    // FIXME: fail if licenceNumber is already used with another account
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify car data is valid   
    $result = isOwnerValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: fail with 400 if record does not exist
    $hasOldOwner = DB::queryFirstField("SELECT COUNT(*) FROM owners WHERE id=%i AND licenceNumber=%i", $authOwner['id'], $licenceNumber);
    if (!$hasOldOwner) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('owners', $data, 'licenceNumber=%i', $licenceNumber);
    echo json_encode(true);
});



// Retrieve all cars, one of the already existed owner
$app->get('/cars', function() use ($app, $log) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $carsList = DB::query("SELECT * FROM cars WHERE ownerId=%i", $authOwner['id']);
    echo json_encode($carsList, JSON_PRETTY_PRINT);
});


// Create a new car, by entering task, dueDate and isDone, one of the already existed owner
$app->post('/cars', function() use ($app, $log) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    $result = iscarValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: verify car data is valid
    $data['ownerId'] = $authOwner['id'];
    DB::insert('cars', $data);
    $app->response()->status(201);
    $id = DB::insertId();
    $log->debug("car created with id=" . $id);
    echo json_encode($id);
});


// Retrieve one car from its id, one of the already existed owner
$app->get('/cars/:id', function($id) use ($app) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $car = DB::queryFirstRow("SELECT * FROM cars WHERE id=%i AND ownerId=%i", $id, $authOwner['id']);
    if ($car) {
        echo json_encode($car, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});


// Update a car, find car by id, can change task, dueDate and isDone
$app->put('/cars/:id', function($id) use ($app) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify car data is valid   
    $result = iscarValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // FIXME: fail with 400 if record does not exist
    $hasOldcar = DB::queryFirstField("SELECT COUNT(*) FROM cars WHERE id=%i AND ownerId=%i", $id, $authOwner['id']);
    if (!$hasOldcar) {
        echo json_encode("404 - unable to update non-existing record");
        $app->response()->status(404);
        return;
    }
    DB::update('cars', $data, 'id=%d AND ownerId=%i', $id, $authOwner['id']);
    echo json_encode(true);
});

// Delete a car, find car by id, delete it
$app->delete('/cars/:id', function($id) {
    $authOwner = getAuthOwner();
    if ($authOwner === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    // MAYBE-car: if record exists and it's not authOwner's then return 403 instead
    DB::delete('cars', 'id=%i AND ownerId=%i', $id, $authOwner['id']);
    echo json_encode(DB::affectedRows() != 0);
});

$app->run();
